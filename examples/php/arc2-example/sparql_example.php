<?php

/**
 * ARC2 example that shows how to import data into an internal SPARQL repository and query it.
 */

// NOTE: needs a MySQL server up and running!

require 'vendor/autoload.php';

// init triple store
$config = array(
    /* db */
    'db_host' => '127.0.0.1',
    'db_name' => 'sparql_example',
    'db_user' => 'root',
    'db_pwd' => 'root',
    /* store */
    'store_name' => 'sparql_example',
);
$store = ARC2::getStore($config);
$store->setUp();

// load external RDF document
$store->query("LOAD <http://dig.csail.mit.edu/2008/webdav/timbl/foaf.rdf>");

$query =
<<<QUERY
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    SELECT ?person ?name
    WHERE {
        <http://www.w3.org/People/Berners-Lee/card#i> foaf:knows ?person .
        OPTIONAL { ?person foaf:name ?name }
    }
QUERY;

$triples = $store->query($query, 'rows');
if (!$store->getErrors()) {
    foreach ($triples as $i=>$triple) {
        print !is_null($triple['name']) ? $triple['name'] : '?';
        print " <{$triple['person']}>";
        print PHP_EOL;
    }
} else {
    // error handling (`$store->getErrors()` returns an array of error messages)
}

// cleanup
$store->reset();
