<?php

/**
 * ARC2 example that shows how to read/manipulate/write RDF graphs
 */

require 'vendor/autoload.php';

define('FOAF', 'http://xmlns.com/foaf/0.1/');

$namespaces = array(
    'foaf' => FOAF,
);

// - read RDF data from RDF file
$parser = ARC2::getRDFXMLParser();
$parser->parse('foaf.rdf');
$triples = $parser->getTriples();
/*
 * `$parser->getTriples()` generates an array of triples (for statement-centric processing) in the form:
 *
 * array(
 *     array(
 *         's' => '<subject>',
 *         'p' => '<predicate>',
 *         'o' => '<object>',
 *         's_type' => '<subject type (e.g. uri, bnode)>',
 *         'o_type' => '<object type (e.g. uri, bnode, literal)>',
 *         'o_datatype' => '<object's datatype>',
 *         'o_lang' => '<object's language>',
 *     ),
 *     //...
 * );
 */

// - find resource that represents me
$index = ARC2::getSimpleIndex($triples, false); // false -> create non-flattened resource index
$me =& $index['http://kindl.io/christoph/foaf.rdf#me'];
/*
 * `ARC2::getSimpleIndex($triples, false)` converts an array of triples to an extended index
 * (for resource-centric processing) in the form:
 *
 * array(
 *     '<subject#1>' => array(
 *         '<predicate#1>' => array(
 *             array(
 *                 'value' => '<object#1's value>',
 *                 'type' => '<object#1's type (e.g. uri, bnode, literal)>',
 *                 'datatype' => '<object#1's datatype if type = literal>',
 *                 'language' => '<object#1's language if type = literal>',
 *             ),
 *             //...
 *         ),
 *         //...
 *     ),
 *     //...
 * );
 */

// - add new properties
$me[FOAF.'mbox'][] = array(
    'value' => 'mailto:e0828633@student.tuwien.ac.at',
    'type' => 'uri',
);
$me[FOAF.'nick'] = array(
    array(
        'value' => 'ckristo',
        'type' => 'literal',
    ),
);

// - alter existing statement
$me[FOAF.'age'][0]['value'] = '25';

// - find some existing statements and iterate over them
foreach ($me[FOAF.'mbox'] as $i=>$object) {
    print $object['value'] . PHP_EOL;
}

// alternative: use of ARC2's Resource helper class
// -- provides several helper functions to operate on resource index
$me2 = ARC2::getResource(array('ns' => $namespaces));
$me2->setIndex($index);
$me2->setUri('http://kindl.io/christoph/foaf.rdf#me');
print $me2->getPropValue('foaf:name') . PHP_EOL;

// - delete some statements
unset($me[FOAF.'mbox']);

// - print modified RDF document
print $parser->toRDFXML($index, $namespaces);
