<?php

/**
 * EasyRdf example that shows how to query a remote SPARQL endpoint.
 */

require 'vendor/autoload.php';

$sparql_client = new EasyRdf_Sparql_Client("http://dbpedia.org/sparql");

$query =
<<<QUERY
    PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
    PREFIX dbpprop: <http://dbpedia.org/property/>
    SELECT ?country ?population
    WHERE {
        ?country a dbpedia-owl:Country .
        ?country dbpprop:populationCensus ?population .
        FILTER ( isLiteral(?population) && ?population > 50000000 )
    }
    ORDER BY DESC(?population)
    LIMIT 5
QUERY;

try {
    $result_set = $sparql_client->query($query);
    foreach ($result_set as $result) {
        print "<{$result->country}> {$result->population}" . PHP_EOL;
    }
} catch (Exception $ex) {
    // error handling
}
