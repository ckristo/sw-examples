<?php

/**
 * EasyRdf example that shows how to read/manipulate/write RDF graphs
 */

require 'vendor/autoload.php';

// - provide custom resource type mapping class for foaf:Person
class Foaf_Person extends EasyRdf_Resource {

    /**
     * Returns the full name of the Person
     * @return string the full name of the Person
     */
    public function getFullName() {
        if ($this->hasProperty("foaf:name")) {
            return $this->get("foaf:name");
        } else if ($this->hasProperty("foaf:givenName") && $this->hasProperty("foaf:familyName")) {
            return $this->get("foaf:givenName") . " " . $this->get("foaf:familyName");
        } else {
            return '';
        }
    }
}
EasyRdf_TypeMapper::set("foaf:Person", "Foaf_Person");

$graph = new EasyRdf_Graph();
$graph->parseFile("foaf.rdf");

// - find resource that represents me
$me = $graph->resource('http://kindl.io/christoph/foaf.rdf#me');

// - add new properties
$me->add("foaf:mbox", new EasyRdf_Resource("mailto:e0828633@student.tuwien.ac.at"));
$me->add("foaf:nick", new EasyRdf_Literal("ckristo"));

// - alter existing statement
$me->set("foaf:age", new EasyRdf_Literal(25, null, "xsd:integer"));

// - find some existing statements and iterate over them
foreach ($me->all("foaf:mbox") as $mbox) {
    print $mbox . PHP_EOL;
}

// - use custom resource type mapping
print "My full name is: " . $me->getFullName() . PHP_EOL;

// - delete some statements
$me->delete("foaf:mbox");

// - print modified RDF document
print $graph->serialise("rdfxml");
