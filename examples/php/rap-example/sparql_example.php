<?php

/**
 * RAP (RDF-API for PHP) example that shows how to query a model using SPARQL.
 */

define('RDFAPI_INCLUDE_DIR', './vendor/rdfapi-php/api/');
include RDFAPI_INCLUDE_DIR . 'RDFAPI.php';

// - create model and load data
$model = ModelFactory::getDefaultModel();
$model->load("http://dig.csail.mit.edu/2008/webdav/timbl/foaf.rdf");

$query =
<<<QUERY
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    SELECT ?person ?name
    WHERE {
        <http://www.w3.org/People/Berners-Lee/card#i> foaf:knows ?person .
        OPTIONAL { ?person foaf:name ?name }
    }
QUERY;

// - execute SPARQL query and obtain result set
$result = array();
try {
    $result = $model->sparqlQuery($query);
} catch (Exception $ex) {
    // error handling
}

// - iterate over result set
foreach ($result as $row) {
    $person = $row['?person']->getURI();
    $name   = ($row['?name']) ? $row['?name']->getLabel() : '?';
    print "$name <$person>" . PHP_EOL;
}
