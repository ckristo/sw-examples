<?php

/**
 * RAP (RDF-API for PHP) example that shows how to read/manipulate/write RDF graphs with RAP's resource-centric API.
 */

define('RDFAPI_INCLUDE_DIR', './vendor/rdfapi-php/api/');
include RDFAPI_INCLUDE_DIR . 'RDFAPI.php';
include RDFAPI_INCLUDE_DIR . 'resModel/ResResource.php';
include RDFAPI_INCLUDE_DIR . 'resModel/ResProperty.php';
include RDFAPI_INCLUDE_DIR . 'resModel/ResLiteral.php';

# - define constants for used namespaces
define('FOAF', 'http://xmlns.com/foaf/0.1/');
define('XSD', 'http://www.w3.org/2001/XMLSchema#');

// - load external RDF file into a memory model
$model = ModelFactory::getResModel(MEMMODEL);
$model->load("foaf.rdf");

// - add properties
$me = $model->createResource('http://kindl.io/christoph/foaf.rdf#me');
$me->addProperty($model->createProperty(FOAF.'mbox'), $model->createResource('mailto:e0828633@student.tuwien.ac.at'));
$me->addProperty($model->createProperty(FOAF.'nick'), $model->createLiteral('ckristo'));

// - alter an existing statement
$me->removeAll($model->createResource(FOAF.'age'));
$me->addProperty($model->createProperty(FOAF.'age'), $model->createTypedLiteral(25, XSD.'integer'));

// - find some existing statements and iterate over them
foreach ($me->listProperties($model->createResource(FOAF.'mbox')) as $stmt) {
    print $stmt->getObject()->getURI() . PHP_EOL;
}

// - delete some statements
$me->removeAll($model->createResource(FOAF.'mbox'));

// - print modified RDF document
print $model->writeRDFtoString();
