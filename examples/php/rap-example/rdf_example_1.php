<?php

/**
 * RAP (RDF-API for PHP) example that shows how to read/manipulate/write RDF graphs with RAP's statement-centric API.
 */

define('RDFAPI_INCLUDE_DIR', './vendor/rdfapi-php/api/');
include RDFAPI_INCLUDE_DIR . 'RDFAPI.php';

# - define constants for used namespaces
define('FOAF', 'http://xmlns.com/foaf/0.1/');
define('XSD', 'http://www.w3.org/2001/XMLSchema#');

// - load external RDF file into a memory model
$model = ModelFactory::getDefaultModel();
$model->load("foaf.rdf");

// - add properties
$me = new Resource('http://kindl.io/christoph/foaf.rdf#me');
$model->add(new Statement($me, new Resource(FOAF.'mbox'), new Resource('mailto:e0828633@student.tuwien.ac.at')));
$model->add(new Statement($me, new Resource(FOAF.'nick'), new Literal('ckristo')));

// - alter an existing statement
$stmt = $model->findFirstMatchingStatement($me, new Resource(FOAF.'age'), NULL);
$model->remove($stmt);
$model->add(new Statement($me, new Resource(FOAF.'age'), new Literal(25, NULL, XSD.'integer')));

// - find some existing statements and iterate over them
$iter = $model->findAsIterator($me, new Resource(FOAF.'mbox', NULL));
while ($iter->hasNext()) {
    $stmt = $iter->next();
    print $stmt->getObject()->getURI() . PHP_EOL;
}

// - delete some statements
$iter = $model->findAsIterator($me, new Resource(FOAF.'mbox', NULL));
while ($iter->hasNext()) {
    $model->remove($iter->next());
}

// - print modified RDF document
print $model->writeRDFtoString();
