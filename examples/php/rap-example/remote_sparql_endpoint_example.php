<?php

/**
 * RAP (RDF-API for PHP) example that shows how to query a remote SPARQL endpoint.
 */

define('RDFAPI_INCLUDE_DIR', './vendor/rdfapi-php/api/');
include RDFAPI_INCLUDE_DIR . 'RDFAPI.php';

$client = ModelFactory::getSparqlClient('http://dbpedia.org/sparql');

// define query
$queryStr =
<<<QUERY
    PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
    PREFIX dbpprop: <http://dbpedia.org/property/>
    SELECT ?country ?population
    WHERE {
        ?country a dbpedia-owl:Country .
        ?country dbpprop:populationCensus ?population .
        FILTER ( isNumeric(?population) && ?population > 50000000 )
    }
    ORDER BY DESC(?population)
    LIMIT 5
QUERY;

$query = new ClientQuery();
$query->query($queryStr);

// - execute SPARQL query and obtain result set
$result = array();
try {
    $result = $client->query($query);
} catch (Exception $ex) {
    // error handling
}

// - iterate over result set
foreach ($result as $row) {
    print "<{$row['?country']->getURI()}> {$row['?population']->getLabel()}" . PHP_EOL;
}
