var fs = require('fs');
var rdf = require('rdf-ext');

// - include store implementations
var InMemoryStore = require('rdf-store-inmemory');

// - register prefixes
rdf.setPrefix('foaf','http://xmlns.com/foaf/0.1/');

// - register parsers
var RdfXmlParser = require('rdf-parser-rdfxml');
rdf.parsers['application/rdf+xml'] = RdfXmlParser;
// NOTE: alternatively, use the 'rdf-formats-common' package

fs.readFile('foaf.rdf', function (err, data) {
    if (err) { /* error handling */ }

    // - parses the RDF/XML file
    rdf.parsers.parse('application/rdf+xml', data.toString(), function(err, graph) {
        if (err) { /* error handling */ }

        // NOTE: demonstrates on how to use the RDF Interfaces API ::

        var meURI = 'http://kindl.io/christoph/foaf.rdf#me';

        // - add new properties
        var me = rdf.createNamedNode(meURI);
        graph.add(rdf.createTriple(me,
            rdf.createNamedNode(rdf.resolve('foaf:mbox')),
            rdf.createNamedNode('mailto:e0828633@student.tuwien.ac.at')));
        graph.add(rdf.createTriple(me,
            rdf.createNamedNode(rdf.resolve('foaf:nick')),
            rdf.createLiteral('ckristo')));

        // - alter an existing statement
        graph.removeMatches(me, rdf.createNamedNode(rdf.resolve('foaf:age')), null);

        graph.add(rdf.createTriple(me,
            rdf.createNamedNode(rdf.resolve('foaf:age')),
            rdf.createLiteral('25', null, rdf.resolve('xsd:integer'))));

        // - find some existing statements and iterate over them
        graph.match(me, rdf.createNamedNode(rdf.resolve('foaf:mbox')), null)
            .forEach(function(triple) {
                console.log(triple.object.toString());
            });

        // - delete some statements
        graph.removeMatches(me, rdf.createNamedNode(rdf.resolve('foaf:mbox')), null);

        // - print modified RDF graph
        console.log(graph.toString());

        // NOTE: demonstrates how to use the Store API ::

        var graphURI = 'http://kindl.io/christoph/foaf.rdf';

        // - create a new store
        var store = new InMemoryStore();

        // - add a named graph to the store
        store.add(graphURI, graph);

        // - retrieve a graph from a store
        store.graph(graphURI, function(err, graph) {
            if (err) { /* error handling */ }
            // ...
        });
        // alternatively with the use of Promise API
        store.graph(graphURI)
            .then(function(graph) {
                // ...
            });

        // - retrieve matching triples from a graph in the store
        store.match(me, rdf.createNamedNode(rdf.resolve('foaf:nick')), null, graphURI, function(err, graph) {
            if (err) { /* error handling */ }
            // ...
        });

        // - remove matching triples from a graph in the store
        store.removeMatches(me, rdf.createNamedNode(rdf.resolve('foaf:nick')), null, graphURI)
            .then(function() {
                store.graph(graphURI)
                    .then(function(graph) {
                        // NOTE: graph does not contain `foaf:nick` triples any longer
                        // ...
                    })
            })

    });
});
