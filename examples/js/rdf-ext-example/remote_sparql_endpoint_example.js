var rdf = require('rdf-ext');

// - include parser implementations
var RdfXmlParser = require('rdf-parser-rdfxml');

// - include store implementations
var SparqlStore = require('rdf-store-sparql');

// - register prefixes
rdf.setPrefix('foaf','http://xmlns.com/foaf/0.1/');

/*
// - overwrite BlankNode constructor to allow defining a custom label (required by SPARQL JSON parser)
rdf.BlankNode = function(label) {
    this.interfaceName = 'BlankNode';
    if (label != null) {
        this.nominalValue = label;
    } else {
        this.nominalValue = 'b' + (++rdf.BlankNode.nextId);
    }
};
*/

// - register parser for SPARQL JSON results format.
/**
 * A custom parser for parsing SPARQL JSON results.
 * @type DataParser
 */
rdf.parsers['application/sparql-results+json'] = {

    /**
     * Map used to convert a SPARQL result JSON binding to a RDF-Ext model instance.
     */
    'resultTypeMapping' : {
        'uri' : function(binding) {
            return rdf.createNamedNode(binding.value);
        },
        'literal' : function(binding) {
            return rdf.createLiteral(binding.value, binding['xml:lang'], binding.datatype);
        },
        'typed-literal' : function(binding) {
            return this.literal(binding);
        },
        'bnode' : function(binding) {
            return rdf.createBlankNode(binding.value);
        }
    },

    /**
     * Custom parse function for SPARQL results returned as JSON.
     * NOTE: does not parse to a graph object but rather to a plain Javascript array.
     *
     * @param data the JSON string to parse
     * @param callback a callback function (uses node calling convention)
     * @returns {Promise}
     */
    'parse' : function(data, callback) {
        var self = this;
        return new Promise(function(resolve, reject) {
            var dataJSON = {};
            try {
                dataJSON = JSON.parse(data);
            } catch(err) {
                // - Callback API
                if (callback) {
                    callback(err);
                }
                // - Promises API
                else {
                    reject(err);
                }
                return;
            }

            // - map to correct JS types
            var mappedResults = [];
            for (var i in dataJSON.results.bindings) {
                var result = dataJSON.results.bindings[i];
                var mappedResult = {};
                for (var j in dataJSON.head.vars) {
                    var variable = dataJSON.head.vars[j];
                    if (!result[variable]) {
                        mappedResult[variable] = null;
                    } else {
                        mappedResult[variable] = self.resultTypeMapping[result[variable].type](result[variable]);
                    }
                }
                mappedResults.push(mappedResult);
            }

            // - Callback API
            if (callback) {
                callback(null, mappedResults);
            }
            // - Promises API
            else {
                resolve(mappedResults);
            }
        });
    }
};

store = new SparqlStore({
    'endpointUrl': 'http://dbpedia.org/sparql',
});

// NOTE: SPARQL store does not support issuing conventional SPARQL SELECT queries,
//       but its SparqlHttp object store.client does!

// - issue select query using RDF-Ext's `sparql-http-client` library
var query =
    'PREFIX dbpedia-owl: <http://dbpedia.org/ontology/> ' +
    'PREFIX dbpprop: <http://dbpedia.org/property/>' +
    'SELECT ?country ?population ' +
    'WHERE { ' +
    '    ?country a dbpedia-owl:Country . ' +
    '    ?country dbpprop:populationCensus ?population . ' +
    '    FILTER ( isNumeric(?population) && ?population > 50000000 ) ' +
    '} ' +
    'ORDER BY DESC(?population) ' +
    'LIMIT 5';
store.client.selectQuery(query)
    .then(function(response) {
        // - check response status code
        if (response.statusCode < 200 || response.statusCode >= 300) {
            var err = new Error('status code: ' + response.statusCode);
            err.statusCode = response.statusCode;
            throw err;
        }
        // - parse result
        return rdf.parsers.parse(response.headers['content-type'], response.content);
    })
    .then(function(result) {
        for (var i in result) {
            console.log('<' + result[i]['country'].toString() + '> ' + result[i]['population'].toString());
        }
    })
    .catch(function(err) {
        /* error handling */
        // ...
    });
