(function() {
    "use strict";

    // - write console output to Browser page too
    var console_log = console.log;
    console.log = function (message) {
        $('#output').append($('<div/>', { 'text' : message }));
        console_log.apply(console, arguments);
    };

    // ########## rdfquery relevant code :: ##########

    // - create empty $.rdf instance
    var rdf = $.rdf();

    // - load external RDF/XML document
    $.get('timbl_foaf.rdf')
        // NOTE: timbl_foaf.rdf is a copy of http://dig.csail.mit.edu/2008/webdav/timbl/foaf.rdf
        //       -- cannot load cross doain resources because of Browser's same origin policy
        .done(function(data, textStatus, jqXHR) {
            if (textStatus !== 'success') { /* error handling */ }

            // - insert loaded document into databank
            rdf.load(data);

            // - filter triples (based on the query shown below)
            var filtered = rdf
                .prefix('foaf', 'http://xmlns.com/foaf/0.1/')
                .where('<http://www.w3.org/People/Berners-Lee/card#i> foaf:knows ?person .')
                .where('?person foaf:name ?name .', { optional : true });
            // - equivalent to:
            /* PREFIX foaf: <http://xmlns.com/foaf/0.1/>
               SELECT ?person ?name
               WHERE {
                 <http://www.w3.org/People/Berners-Lee/card#i> foaf:knows ?person .
                 OPTIONAL { ?person foaf:name ?name . }
               } */

            // - project the result on needed variables
            var results = filtered.select(['person', 'name']);

            var desc = filtered.describe(['?person']);

            // - print result
            results.forEach(function(result) {
                var name = (result['name']) ? result['name'].value : '?';
                var person = result['person'].value;
                console.log(name + " <" + person + ">");
            });
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            /* error handling */
        });
})();
