(function($) {
    "use strict";

    // - write console output to Browser page too
    var console_log = console.log;
    console.log = function (message) {
        $('#output').append($('<div/>', { 'text' : message }));
        console_log.apply(console, arguments);
    };

    $(document).ready(function() {
        // - parse RDFa out of HTML #input element
        var rdf = $('#input').rdf();

        // - serialize to Turtle
        var output = rdf.databank.dump({ 'format' : 'text/turtle', serialize: true });
        console.log(output);
    });
})(jQuery);
