(function() {
    "use strict";

    // - write console output to Browser page too
    var console_log = console.log;
    console.log = function (message) {
        $('#output').append($('<div/>', { 'text' : message }));
        console_log.apply(console, arguments);
    };

    // ########## rdfquery relevant code :: ##########

    // - define namespaces
    var ns = {
        'foaf' : 'http://xmlns.com/foaf/0.1/',
        'xsd' : 'http://www.w3.org/2001/XMLSchema#'
    };

    // - create empty $.rdf instance
    var rdf = $.rdf({ 'namespaces' : ns });

    // - load external RDF/XML document
    $.get('foaf.rdf')
        .done(function(data, textStatus, jqXHR) {
            if (textStatus !== 'success') { /* error handling */ }

            // - insert loaded document into databank
            rdf.load(data);

            // - add new triples
            var me = '<http://kindl.io/christoph/foaf.rdf#me>';
            rdf.add(me+' foaf:mbox <mailto:e0828633@student.tuwien.ac.at> .');
            rdf.add(
                $.rdf.triple(
                    $.rdf.resource(me),
                    $.rdf.resource('foaf:nick', { 'namespaces' : ns }), // NOTE: `options.namespaces` need to be defined explicitly, no link to the ones set on `$.rdf` instantiation (cf. line 20)
                    $.rdf.literal('"ckristo"')
                )
            );

            // - alter existing statements
            rdf.where(me+' foaf:age ?x')
                .remove(me+' foaf:age ?x');
            rdf.add(me+' foaf:age "25"^^xsd:integer');

            // - find some existing statements and iterate over them
            rdf.where(me+' foaf:mbox ?mbox').each(function() {
                var mbox = this.mbox.value;
                console.log(mbox.toString());
            });

            // - delete some statements
            var p = me+' foaf:mbox ?x';
            rdf.where(p)
                .remove(p);

            // - print modified RDF document
            console.log( rdf.databank.dump({ 'format' : 'application/rdf+xml', serialize: true }) );
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            /* error handling */
        });
})();
