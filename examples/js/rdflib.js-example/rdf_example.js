var fs = require('fs');
var $rdf = require('rdflib');

FOAF = $rdf.Namespace('http://xmlns.com/foaf/0.1/');
XSD  = $rdf.Namespace('http://www.w3.org/2001/XMLSchema#');

// - create an empty store
var kb = $rdf.graph();

// - load RDF file
fs.readFile('foaf.rdf', function (err, data) {
    if (err) { /* error handling */ }

    // - parse RDF file
    $rdf.parse(data.toString(), kb, undefined, 'application/rdf+xml', function(err, kb) {
        if (err) { /* error handling */ }

        var me = kb.sym('http://kindl.io/christoph/foaf.rdf#me');

        // - add new properties
        kb.add(me, FOAF('mbox'), kb.sym('mailto:e0828633@student.tuwien.ac.at'));
        kb.add(me, FOAF('nick'), kb.literal('ckristo'));

        // - alter existing statement
        kb.removeMany(me, FOAF('age'), undefined, 1);
        kb.add(me, FOAF('age'), kb.literal(25, null, XSD('integer')));
        // NOTE: plain JS value `25` as 3rd param would be equivalent, will be converted automatically by `$rdf.term()`

        // - find some existing statements and iterate over them
        var objs = kb.each(me, FOAF('mbox'), undefined);
        objs.forEach(function(obj) {
            console.log(obj.uri);
        });

        // - delete some statements
        kb.removeMany(me, FOAF('mbox'));

        // - print modified RDF document
        $rdf.serialize(undefined, kb, undefined, 'application/n-quads', function(err, str) {
            console.log(str);
        });
    });
});
