
var $rdf = require('rdflib');

// - create an empty store
var kb = $rdf.graph();

// - load remote RDF document
var fetcher = $rdf.fetcher(kb, 1000);
fetcher.nowOrWhenFetched('http://dig.csail.mit.edu/2008/webdav/timbl/foaf.rdf', function(success, errorMsg, requestResponseData) {
    if (!success) { /* error handling */ }

    var sparqlQuery = 'PREFIX foaf: <http://xmlns.com/foaf/0.1/> \
                       SELECT ?person ?name \
                       WHERE { \
                         <http://www.w3.org/People/Berners-Lee/card#i> foaf:knows ?person . \
                         OPTIONAL { ?person foaf:name ?name . } \
                       }';

    // - create query instance from SPARQL query string
    var query = $rdf.SPARQLToQuery(sparqlQuery, true /* disables resource fetching */, kb);

    kb.fetcher = null; /* disables resource fetching */
    // NOTE: rdflib.js will fetch all resources by default it seems
    // which will issue errors when a resource cannot be parsed

    // - execute SPARQL query and obtain result set
    kb.query(query, function(result) {
        var name = (result['?name']) ? result['?name'].value : '?';
        var person = result['?person'].uri ? result['?person'].uri : result['?person'].toString();
        console.log(name + " <" + person + ">");
    });

});
