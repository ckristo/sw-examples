var fs  = require('fs');
var rdf = require('rdf');

// - extends built-in JS types for use with node-rdf
rdf.setBuiltins();

var rdfEnv = rdf.environment;

// - load RDF document file
fs.readFile('foaf.ttl', function (err, data) {
    if (err) { /* error handling */ }

    // - parse the loaded file
    var parser = new rdf.TurtleParser();
    parser.parse(data.toString(), function(graph) {
        var me = 'http://kindl.io/christoph/foaf.rdf#me';

        // - add new properties
        graph.add(rdfEnv.createTriple(me, 'foaf:mbox', 'mailto:e0828633@student.tuwien.ac.at'));
        graph.add(rdfEnv.createTriple(me, 'foaf:nick', 'ckristo'));

        /*
        // - alter an existing statement
        graph.removeMatches(me, rdfEnv.createNamedNode('foaf:age'), null);
        graph.add(rdfEnv.createTriple(me,
            rdfEnv.createNamedNode('foaf:age'),
            rdfEnv.createLiteral('25', null, rdfEnv.createNamedNode('xsd:integer'))));

        // - find some existing statements and iterate over them
        graph.match(me, rdfEnv.createNamedNode('foaf:mbox'), null).forEach(function (triple) {
            console.log(triple.object.toString());
        });

        // - delete some statements
        graph.removeMatches(me, rdfEnv.createNamedNode('foaf:mbox'), null);

        // - print modified RDF graph
        console.log(graph.toNT());
        */

        console.log(graph.toString());
    });
});
