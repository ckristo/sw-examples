var fs = require('fs');
var rdfstore = require('rdfstore');

// NOTE: rdfstore-js doesn't support RDF/XML parsing at the moment

rdfstore.create(function (err, store) {
    if (err) { /* error handling */ }

    var rdfEnv = store.rdf;

    var me = rdfEnv.createNamedNode('http://kindl.io/christoph/foaf.rdf#me');

    // - read file and add triples of the file to store
    fs.readFile('foaf.ttl', function (err, data) {
        if (err) { /* error handling */ }

        // - load RDF data into store
        store.load('text/turtle', data.toString(), function (err, result) {
            if (err) { /* error handling */ }

            store.graph(function (err, graph) {
                if (err) { /* error handling */ }

                // NOTE: this operations will not alter the actual store content, but will use
                //       the RDF Interfaces API to work with the (temp.) graph instance ::

                // - add new properties
                graph.add(rdfEnv.createTriple(me,
                    rdfEnv.createNamedNode('foaf:mbox'),
                    rdfEnv.createNamedNode('mailto:e0828633@student.tuwien.ac.at')));
                graph.add(rdfEnv.createTriple(me,
                    rdfEnv.createNamedNode('foaf:nick'),
                    rdfEnv.createLiteral('ckristo')));

                // - alter an existing statement
                graph.removeMatches(me, rdfEnv.createNamedNode('foaf:age'), null);
                graph.add(rdfEnv.createTriple(me,
                    rdfEnv.createNamedNode('foaf:age'),
                    rdfEnv.createLiteral('25', null, rdfEnv.createNamedNode('xsd:integer'))));

                // - find some existing statements and iterate over them
                graph.match(me, rdfEnv.createNamedNode('foaf:mbox'), null).forEach(function (triple) {
                    console.log(triple.object.toString());
                });

                // - delete some statements
                graph.removeMatches(me, rdfEnv.createNamedNode('foaf:mbox'), null);

                // - print modified RDF graph
                console.log(graph.toNT());
            });

            // NOTE: this operations will alter the store's content ::

            // - create a temp. graph instance that contains the triples to insert
            var g = rdfEnv.createGraph();
            g.add(rdfEnv.createTriple(me,
                rdfEnv.createNamedNode('foaf:mbox'),
                rdfEnv.createNamedNode('mailto:e0828633@student.tuwien.ac.at')));
            g.add(rdfEnv.createTriple(me,
                rdfEnv.createNamedNode('foaf:nick'),
                rdfEnv.createLiteral('ckristo')));

            // - insert triples
            store.insert(g, function(err, result) {
                // - create a temp. graph instance that contains the triples to delete
                var g = rdfEnv.createGraph();
                g.add(rdfEnv.createTriple(me,
                    rdfEnv.createNamedNode('foaf:mbox'),
                    rdfEnv.createNamedNode('mailto:christoph.kindl@student.tuwien.ac.at')));
                g.add(rdfEnv.createTriple(me,
                    rdfEnv.createNamedNode('foaf:mbox'),
                    rdfEnv.createNamedNode('mailto:christoph@kindl.io')));

                store.delete(g, function(err, result) {
                    if (err) { /* error handling */ }
                    // ...
                });
            });

            // NOTE: store.insert and store.delete are convenience methods which create and execute SPARQL queries
            // on the fly, hence store.execute could directly be used.
        });
    });
});
