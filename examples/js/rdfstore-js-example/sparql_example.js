var rdfstore = require('rdfstore');

rdfstore.create(function (err, store) {
    if (err) { /* error handling */ }

    // NOTE: rdfstore-js doesn't support RDF/XML parsing at the moment -->
    //   <https://ckristo.net/sw-examples/timbl-foaf.ttl> is a Turtle version
    //   of <http://dig.csail.mit.edu/2008/webdav/timbl/foaf.rdf>

    // - load remote RDF document
    store.execute('LOAD <https://ckristo.net/sw-examples/timbl-foaf.ttl>', function (success, num_triples) {
        if (!success) { /* error handling */ }

        var query = 'PREFIX foaf: <http://xmlns.com/foaf/0.1/> \
                     SELECT ?person ?name \
                     FROM <https://ckristo.net/sw-examples/timbl-foaf.ttl> \
                     WHERE { \
                       <http://www.w3.org/People/Berners-Lee/card#i> foaf:knows ?person . \
                       OPTIONAL { ?person foaf:name ?name } \
                     }';

        // - execute SPARQL query and obtain result set
        store.execute(query, function (err, results) {
            if (err) { /* error handling */ }

            // - iterate over result set
            results.forEach(function(result) {
                var name = (result.name) ? result.name.value : '?';
                var person = result.person.value;
                console.log(name + " <" + person + ">");
            });
        });
    });
});
