#!/usr/bin/env ruby

=begin
  example that shows how to read/manipulate/write RDF data with Spira
=end

require 'spira'
require 'rdf/vocab'
require 'rdf/rdfxml'

# - define a model class for foaf:Person
class Person < Spira::Base
  type RDF::Vocab::FOAF.Person
  property :name,   :predicate => RDF::Vocab::FOAF.name,  :type => String
  property :age,    :predicate => RDF::Vocab::FOAF.age,   :type => Integer
  property :nick,   :predicate => RDF::Vocab::FOAF.nick,  :type => String
  has_many :mboxes, :predicate => RDF::Vocab::FOAF.mbox,  :type => URI
  has_many :knows,  :predicate => RDF::Vocab::FOAF.knows, :type => :person

  # - a dummy function which shows that you can add custom business functions to Spira models.
  def say_hi
    puts "#{self.name} says 'Hi'!"
  end
end

# - create RDF repository and load RDF document
Spira.repository = RDF::Repository.load('foaf.rdf')

# - instantiate Spira model
me = RDF::URI('http://kindl.io/christoph/foaf.rdf#me').as(Person) # or:
#me = Person.for RDF::URI('http://kindl.io/christoph/foaf.rdf#me')

# - add new properties
me.mboxes << RDF::URI('mailto:e0828633@student.tuwien.ac.at')
me.nick = 'ckristo'

# - alter an existing statement
me.age = 25

# - call custom method `say_hi`
me.say_hi

# - find some existing statements and iterate over them
me.mboxes.each do |mbox|
  puts mbox
end
# NOTE: Spira model objects also implement RDF::Enumerable, RDF::Queryable, and RDF::Mutable
# e.g.:
#me.each_statement do |stmt|
#  puts stmt
#end

# - delete some statements
me.mboxes = []

# - populate changes to repository
me.save!

# - print modified RDF graph
puts Spira.repository.dump(:ntriples)
