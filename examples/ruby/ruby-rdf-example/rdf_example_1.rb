#!/usr/bin/env ruby

=begin
  example that shows how to read/manipulate/write RDF graphs with the RDF.rb API
=end

require 'rdf'
require 'rdf/vocab'
require 'rdf/rdfxml'
#require 'rdf/turtle'

# - load RDF document
graph = RDF::Graph.load('foaf.rdf')

# - add new properties
me = RDF::URI('http://kindl.io/christoph/foaf.rdf#me')
graph << RDF::Statement({
  :subject   => me,
  :predicate => RDF::Vocab::FOAF.mbox,
  :object    => RDF::URI('mailto:e0828633@student.tuwien.ac.at'),
})
graph << [ me, RDF::Vocab::FOAF.nick, 'ckristo' ] # shorter alternative to specify a statement

# - alter an existing statement
graph.update([ me, RDF::Vocab::FOAF.age, 25 ]) # 25 == RDF::Literal('25', :datatype => RDF::XSD.integer)

# - find some existing statements and iterate over them
query = RDF::Query.new
query << [ me, RDF::Vocab::FOAF.mbox, :mbox ]
solutions = query.execute(graph)
solutions.each do |solution|
  puts solution.mbox
end

# - delete some statements
graph.delete([ me, RDF::Vocab::FOAF.mbox, nil ])

# - print modified RDF graph
puts graph.dump(:ntriples)

# serialize graph to file
#RDF::Writer.open("out.ttl", format: :ttl) do |writer|
#  writer << graph
#end
