#!/usr/bin/env ruby

=begin
  example that shows how to query an in-memory repository using SPARQL with ruby-rdf/rdf and ruby-rdf/sparql
=end

require 'rdf'
require 'rdf/rdfxml'
require 'sparql'
require 'openssl'

# - load external RDF document
repository = RDF::Repository.load('http://dig.csail.mit.edu/2008/webdav/timbl/foaf.rdf')

# - parse query
query = SPARQL.parse(%q(
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    SELECT ?person ?name
    WHERE {
      <http://www.w3.org/People/Berners-Lee/card#i> foaf:knows ?person .
      OPTIONAL { ?person foaf:name ?name }
    }
))

# - execute query
results = query.execute(repository)

# - iterate over results
results.each do |result|
  person = result[:person].to_s
  name   = result.bound?(:name) ? result[:name].to_s : '?'
  puts "#{name} <#{person}>"
end
