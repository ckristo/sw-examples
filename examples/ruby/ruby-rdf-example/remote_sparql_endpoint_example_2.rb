#!/usr/bin/env ruby

=begin
  example that shows how to query a remote SPARQL endpoint with the SPARQL client's API.
=end

require 'rdf'
require 'sparql/client'

# - define ad-hoc vocabs
DBP_ONT  = RDF::Vocabulary.new('http://dbpedia.org/ontology/')
DBP_PROP = RDF::Vocabulary.new('http://dbpedia.org/property/')

client = SPARQL::Client.new('http://dbpedia.org/sparql')

# - define SPARQL query
query = client.select(:country, :population)
  .where([ :country, RDF.type, DBP_ONT.Country ])
  .where([ :country, DBP_PROP.populationCensus, :population ])
  .filter('isNumeric(?population) && ?population > 50000000')
  .order('DESC(?population)')
  .limit(5)
# - equivalent query =
=begin
<<QUERY
    PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
    PREFIX dbpprop: <http://dbpedia.org/property/>
    SELECT ?country ?population
    WHERE {
        ?country a dbpedia-owl:Country .
        ?country dbpprop:populationCensus ?population .
        FILTER ( isNumeric(?population) && ?population > 50000000 )
    }
    ORDER BY ?population
    LIMIT 5
QUERY
=end

# - execute query and iterate over results
query.each_solution do |result|
  country    = result[:country].to_s
  population = result[:population].to_s
  puts "<#{country}> #{population}"
end
