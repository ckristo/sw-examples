#!/usr/bin/env ruby

=begin
  example that shows how to query a remote SPARQL endpoint with a query given as string.
=end

require 'sparql/client'

client = SPARQL::Client.new('http://dbpedia.org/sparql')

# - execute SPARQL query
results = client.query(%q(
    PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
    PREFIX dbpprop: <http://dbpedia.org/property/>
    SELECT ?country ?population
    WHERE {
        ?country a dbpedia-owl:Country .
        ?country dbpprop:populationCensus ?population .
        FILTER ( isNumeric(?population) && ?population > 50000000 )
    }
    ORDER BY DESC(?population)
    LIMIT 5
))

# - iterate over results
results.each do |result|
  country    = result[:country].to_s
  population = result[:population].to_s
  puts "<#{country}> #{population}"
end
