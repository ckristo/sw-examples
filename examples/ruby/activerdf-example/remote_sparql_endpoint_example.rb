#!/usr/bin/env ruby -W0

=begin
  example that shows how to query a remote SPARQL endpoint.
=end

require 'active_rdf'

# - obtain connection to SPARQL endpoint
adapter = ConnectionPool.add_data_source(:type => :sparql, :url => 'http://dbpedia.org/sparql', :engine => :virtuoso)
adapter.enabled = true

# - register namespaces
Namespace.register(:dbp_ont, 'http://dbpedia.org/ontology/')
Namespace.register(:dbp_prop, 'http://dbpedia.org/property/')

# - construct Ruby classes for RDFS classes
#ObjectManager.construct_classes

# - construct query
query = Query.new \
  .select(:country, :population) \
  .where(:country, RDF::type, DBP_ONT::Country) \
  .where(:country, DBP_PROP::populationCensus, :population) \
  .filter('isNumeric(?population)') \
  .filter_operator(:population, '>', 50000000)
# NOTE: `ORDER BY DESC(?population)` is not supported (cf. query2sparql.rb)
#       -> we sort and limit the result later on (cf. lines 37ff)

# - execute query and collect results
results = []
query.execute { |country, population|
  results << { :country => country, :population => population }
}

# - sort results
results.sort! { |a, b|
  b[:population].to_i <=> a[:population].to_i
}
# - limit results
results = results.shift(5)

# - iterate over results
results.each { |item|
  puts "<#{item[:country].uri}> #{item[:population]}"
}
