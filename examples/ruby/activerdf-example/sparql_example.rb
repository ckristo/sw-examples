#!/usr/bin/env ruby -W0

=begin
  example that shows how to read/manipulate/write RDF graphs with ActiveRDF
=end

# NOTE: This examples requires librdf Ruby bindings to be installed (http://librdf.org/docs/ruby.html)

require 'active_rdf'

# - create in-memory data source
adapter = ConnectionPool.add_data_source(:type => :rdflite)
adapter.enabled = true

# - register namespaces
Namespace.register(:foaf, 'http://xmlns.com/foaf/0.1/')

# - load RDF document
adapter.load('http://dig.csail.mit.edu/2008/webdav/timbl/foaf.rdf') # NOTE: requires librdf Ruby bindings

# - construct Ruby classes for RDFS classes
#ObjectManager.construct_classes

# - construct query
query = Query.new \
  .select(:person) \
  .where(RDFS::Resource.new('http://www.w3.org/People/Berners-Lee/card#i'), FOAF::knows, :person)
# NOTE: `OPTIONAL { ?person foaf:name ?name }` is not supported
#       -> we access the name of the person and check it later on (cf. line 34)

# - execute query and iterate over results
query.execute { |result|
  name = result.foaf::name || '?'
  puts "#{name} <#{result.uri}>"
}
