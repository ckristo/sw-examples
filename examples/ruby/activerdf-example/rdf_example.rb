#!/usr/bin/env ruby -W0

=begin
  example that shows how to read/manipulate/write RDF graphs with ActiveRDF
=end

require 'active_rdf'

# - create in-memory data source
adapter = ConnectionPool.add_data_source(:type => :rdflite)
adapter.enabled = true

# - register namespaces
Namespace.register(:foaf, 'http://xmlns.com/foaf/0.1/')

# - load RDF document
adapter.load('foaf.nt')

# - construct Ruby classes for RDFS classes
#ObjectManager.construct_classes

# - add new properties
me = RDFS::Resource.new('http://kindl.io/christoph/foaf.rdf#me')
me.foaf::nick = 'ckristo'
me.foaf::mbox = me.foaf::mbox << RDFS::Resource.new('mailto:e0828633@student.tuwien.ac.at')
# or:
#adapter.add(me, FOAF::mbox, RDFS::Resource.new('mailto:e0828633@student.tuwien.ac.at'))
#adapter.add(me, FOAF::nick, 'ckristo')

# - alter an existing statement
me.foaf::age = 25

# - find some existing statements and iterate over them
me.foaf::mbox.each { |mbox|
  puts mbox.uri
}

# - delete some statements
adapter.delete(me, FOAF::mbox, nil)

# - print modified graph
puts adapter.dump
