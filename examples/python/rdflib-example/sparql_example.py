#!/usr/bin/env python

"""
example that shows how to query an in-memory repository using SPARQL with RDFLib.
"""

from rdflib import Graph, URIRef
from rdflib.namespace import FOAF
from rdflib.plugins import sparql

# - load external RDF document
graph = Graph()
graph.load('http://dig.csail.mit.edu/2008/webdav/timbl/foaf.rdf')

# - execute query
result = graph.query(
    """PREFIX foaf: <http://xmlns.com/foaf/0.1/>
       SELECT ?person ?name
       WHERE {
         <http://www.w3.org/People/Berners-Lee/card#i> foaf:knows ?person .
         OPTIONAL { ?person foaf:name ?name }
       }""")

# - iterate over results
for row in result:
    person = row.person
    name = row.name or '?'
    print("%s <%s>" % (name, person))

# - create a prepared query
#prepared_query = sparql.prepareQuery('SELECT ?name WHERE { ?person foaf:name ?name}', initNs={'foaf': FOAF})
# - execute prepared query with a binding
#result = graph.query(prepared_query, initBindings={'person': URIRef('http://www.ivan-herman.net/foaf.rdf#me')})
#row = iter(result).next()
#print("Result of prepared query: '%s'" % row.name)
