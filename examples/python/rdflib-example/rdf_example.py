#!/usr/bin/env python

"""
example that shows how to read/manipulate/write RDF graphs with the RDFLib API
"""
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import XSD, FOAF

# - create empty graph
graph = Graph()

# - load RDF document
graph.load('foaf.rdf', format='xml')

# - add new properties
me = URIRef('http://kindl.io/christoph/foaf.rdf#me')
graph.add( (me, FOAF.mbox, URIRef('mailto:e0828633@student.tuwien.ac.at')) )
graph.add( (me, FOAF.nick, Literal('ckristo')) )

# - alter an existing statement
graph.set( (me, FOAF.age, Literal('25', datatype=XSD.integer)) )  # or Literal(25)

# - find some existing statements and iterate over them
for _, _, mbox in graph.triples( (me, FOAF.mbox, None) ):
    print(mbox)

# - iterates over all triples of a graph
#for subj, pred, obj in graph:
#    print(subj, pred, obj)

# - check if a triple is contained in the graph
#if (me, FOAF.nick, None) in graph:
#    print("check")

# - return only specific parts of triples in the graph
#for subj in graph.subjects(FOAF.name, None):
#    print(subj)
#for pred in graph.predicates(me, None):
#   print(pred)
#for obj in graph.objects(me, FOAF.mbox):
#   print(obj)

# - delete some statements
graph.remove( (me, FOAF.mbox, None) )

# - print modified RDF graph
print(graph.serialize(format='nt'))
