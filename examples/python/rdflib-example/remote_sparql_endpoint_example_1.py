#!/usr/bin/env python

"""
example that shows how to query a remote SPARQL endpoint with RDFLib.
"""

# NOTE: requires `urlgrabber` in version 3.9.0

from rdflib import ConjunctiveGraph, Namespace

# - define some namespaces
DBP_ONT = Namespace('http://dbpedia.org/ontology/')
DBP_PROP = Namespace('http://dbpedia.org/property/')

# - create a graph instance that wraps the remote SPARQL endpoint
graph = ConjunctiveGraph('SPARQLStore')  # NOTE: `ConjunctiveGraph` needs to be used here
graph.open('http://dbpedia.org/sparql')

# - execute SPARQL query
result = graph.query(
    """SELECT ?country ?population
       WHERE {
           ?country a dbpedia-owl:Country .
           ?country dbpprop:populationCensus ?population .
           FILTER ( isNumeric(?population) && ?population > 50000000 )
       }
       ORDER BY DESC(?population)
       LIMIT 5""", initNs={'dbpedia-owl': DBP_ONT, 'dbpprop': DBP_PROP})
# NOTE: `initNs` kwarg allows to specify namespaces (analogue SPARQL's PREFIX keyword)

# - iterate over results
for row in result:
    country = row.country
    population = int(row.population)
    print("<%s> %d" % (country, population))
