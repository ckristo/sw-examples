#!/usr/bin/env python

"""
example that shows how to query a remote SPARQL endpoint with RDFLib's SPARQLWrapper.
"""

from SPARQLWrapper import SPARQLWrapper, JSON

sparql = SPARQLWrapper("http://dbpedia.org/sparql")
sparql.setReturnFormat(JSON)  # NOTE: the result of convert() depends on the return format

# - set the query to execute later on
sparql.setQuery(
    """PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
       PREFIX dbpprop: <http://dbpedia.org/property/>
       SELECT ?country ?population
       WHERE {
           ?country a dbpedia-owl:Country .
           ?country dbpprop:populationCensus ?population .
           FILTER ( isNumeric(?population) && ?population > 50000000 )
       }
       ORDER BY DESC(?population)
       LIMIT 5""")

# - execute SPARQL query
results = sparql.query().convert()

# - iterate over the result set
for result in results["results"]["bindings"]:  # NOTE: for JSON return format only!
    country = result["country"]["value"]
    population = int(result["population"]["value"])
    print("<%s> %d" % (country, population))
