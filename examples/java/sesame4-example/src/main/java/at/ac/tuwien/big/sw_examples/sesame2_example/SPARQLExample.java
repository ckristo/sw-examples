package at.ac.tuwien.big.sw_examples.sesame2_example;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.query.*;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.sail.memory.MemoryStore;

/**
 * Example on how to use the OpenRDF Sesame API to query a local repository
 */
public class SPARQLExample {
	
	public static void main(String[] args) {
		// create an empty, in-memory repository
		Repository repository = new SailRepository(new MemoryStore());
		repository.initialize();

        // obtain repository connection
        try(RepositoryConnection repositoryConnection = repository.getConnection()) {
            // read data from RDF document
            try {
                InputStream is = new URL("http://dig.csail.mit.edu/2008/webdav/timbl/foaf.rdf").openStream();
                repositoryConnection.add(is, "http://dig.csail.mit.edu/2008/webdav/timbl/foaf.rdf", RDFFormat.RDFXML);
            } catch (IOException e) {
                /* error handling */
            }

            // evaluate query
            TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL,
                      "PREFIX foaf: <http://xmlns.com/foaf/0.1/> "
                    + "SELECT ?person ?name "
                    + "WHERE { "
                    + "    <http://www.w3.org/People/Berners-Lee/card#i> foaf:knows ?person . "
                    + "    OPTIONAL { ?person foaf:name ?name } "
                    + "}");
			TupleQueryResult result = tupleQuery.evaluate();

            // iterate over result set using Java 8 Stream API
            QueryResults.stream(result)
                    .forEach(bindingSet -> {
                        // get variable bindings
                        Resource person = (Resource) bindingSet.getValue("person");
                        Literal name = (Literal) bindingSet.getValue("name");

                        System.out.print((name == null) ? "?" : name.getLabel());
                        System.out.println(" <"+person+">");
                    });
        } // AUTO-CLOSABLE
	}
}
