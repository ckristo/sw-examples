package at.ac.tuwien.big.sw_examples.sesame2_example;

import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.query.*;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sparql.SPARQLRepository;

/**
 * Example on how to use the OpenRDF Sesame API to query a local repository
 */
public class RemoteSPARQLEndpointExample {
	
	public static void main(String[] args) {
		// create an empty, in-memory repository
		SPARQLRepository repository = new SPARQLRepository("http://dbpedia.org/sparql");
		repository.initialize();

        // obtain repository connection
		try(RepositoryConnection repositoryConnection = repository.getConnection()) {
            // evaluate query
            TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL,
                      "PREFIX dbpedia-owl: <http://dbpedia.org/ontology/> "
                    + "PREFIX dbpprop: <http://dbpedia.org/property/> "
                    + "SELECT ?country ?population "
                    + "WHERE { "
                    + "    ?country a dbpedia-owl:Country . "
                    + "    ?country dbpprop:populationCensus ?population . "
                    + "    FILTER ( isNumeric(?population) && ?population > 50000000 ) "
                    + "} "
                    + "ORDER BY DESC(?population) "
                    + "LIMIT 5");
            TupleQueryResult result = tupleQuery.evaluate();

            // iterate over result set using Java 8 Stream API
            QueryResults.stream(result)
                    .forEach(bindingSet -> {
                        // get variable bindings
                        Resource country = (Resource) bindingSet.getValue("country");
                        Literal population = (Literal) bindingSet.getValue("population");

                        System.out.println("<"+country+"> "+population.getLabel());
                    });
		} // AUTO-CLOSABLE
	}
}
