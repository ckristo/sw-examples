package at.ac.tuwien.big.sw_examples.sesame2_example;

import java.io.IOException;
import java.io.InputStream;

import org.openrdf.model.Model;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.LinkedHashModel;
import org.openrdf.model.impl.SimpleValueFactory;
import org.openrdf.model.vocabulary.FOAF;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.StatementCollector;

/**
 * Example on how to use the OpenRDF Sesame RDF API
 */
public class RDFModelExample {
	
	public static void main(String[] args) {
		// create empty model
		Model model = new LinkedHashModel();
		
		// parse RDF file and add all statements to the model
		RDFParser rdfParser = Rio.createParser(RDFFormat.RDFXML);
		rdfParser.setRDFHandler(new StatementCollector(model));
		
		InputStream is = RDFModelExample.class.getResourceAsStream("/foaf.rdf");
		try {
			rdfParser.parse(is, "file:foaf.rdf");
		} catch (IOException e) {
			/* error handling */
		}

		ValueFactory valueFactory = SimpleValueFactory.getInstance();
		
		Resource subject = valueFactory.createIRI("http://kindl.io/christoph/foaf.rdf#me");
		
		// add new properties
		model.add(subject, FOAF.MBOX, valueFactory.createIRI("mailto:e0828633@student.tuwien.ac.at"));
		model.add(subject, FOAF.NICK, valueFactory.createLiteral("ckristo"));
		
		// alter an existing statement
		model.remove(subject, FOAF.AGE, null);
		model.add(subject, FOAF.AGE, valueFactory.createLiteral(25));

		// find some existing statements and iterate over them
		for (Statement statement : model.filter(subject, FOAF.MBOX, null)) {
			Value mbox = statement.getObject();

			System.out.println(mbox);
		}
		
		// delete some statements
		model.remove(subject, FOAF.MBOX, null);
		
		// print modified RDF document
		Rio.write(model, System.out, RDFFormat.NTRIPLES);
	}
}
