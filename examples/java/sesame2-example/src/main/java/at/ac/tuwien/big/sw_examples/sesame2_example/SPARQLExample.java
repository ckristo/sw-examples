package at.ac.tuwien.big.sw_examples.sesame2_example;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFParseException;
import org.openrdf.sail.memory.MemoryStore;

/**
 * Example on how to use the OpenRDF Sesame API to query a local repository
 */
public class SPARQLExample {
	
	public static void main(String[] args) {
		try {
			// create an empty, in-memory repository
			Repository repository = new SailRepository(new MemoryStore());
			repository.initialize();
			
			// obtain connection to repository
			RepositoryConnection repositoryConnection = repository.getConnection();
			
			try {
				// read data from RDF document
				InputStream is = new URL("http://dig.csail.mit.edu/2008/webdav/timbl/foaf.rdf").openStream();
				repositoryConnection.add(is, "http://dig.csail.mit.edu/2008/webdav/timbl/foaf.rdf", RDFFormat.RDFXML);
				
				TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, 
						  "PREFIX foaf: <http://xmlns.com/foaf/0.1/> "
						+ "SELECT ?person ?name "
						+ "WHERE { "
						+ "    <http://www.w3.org/People/Berners-Lee/card#i> foaf:knows ?person . "
						+ "    OPTIONAL { ?person foaf:name ?name } "
						+ "}");
				
				TupleQueryResult result = tupleQuery.evaluate();
				
				try {
					while (result.hasNext()) {
						BindingSet bindingSet = result.next();
						
						// get variable bindings
						Resource person = (Resource) bindingSet.getValue("person");
						Literal name = (Literal) bindingSet.getValue("name");
						
						System.out.print((name == null) ? "?" : name.getLabel());
						System.out.println(" <"+person+">");
					}
				} catch (Exception e) {
					/* error handling */
				} finally {
					result.close();
				}
			} catch (RDFParseException | IOException e) {
				/* error handling */
			} catch (MalformedQueryException | QueryEvaluationException e) {
				/* error handling */
			} finally {
				repositoryConnection.close();
			}
		} catch (RepositoryException e) { /* error handling */ }
	}
}
