package at.ac.tuwien.big.sw_examples.sesame2_example;

import info.aduna.iteration.Iterations;

import java.io.IOException;
import java.io.InputStream;

import org.openrdf.model.Model;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.LinkedHashModel;
import org.openrdf.model.vocabulary.FOAF;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.Rio;
import org.openrdf.sail.memory.MemoryStore;

/**
 * Example on how to use the OpenRDF Sesame Repository API
 */
public class RDFRepositoryExample {
	
	public static void main(String[] args) {
		try {
			// create an empty, in-memory repository
			Repository repository = new SailRepository(new MemoryStore());
			repository.initialize();
			
			// obtain connection to repository
			RepositoryConnection repositoryConnection = repository.getConnection();
			ValueFactory valueFactory = repositoryConnection.getValueFactory();
			
			try {
				// read data from RDF document
				InputStream is = RDFRepositoryExample.class.getResourceAsStream("/foaf.rdf");
				repositoryConnection.add(is, "file:foaf.rdf", RDFFormat.RDFXML);
				
				Resource subject = valueFactory.createURI("http://kindl.io/christoph/foaf.rdf#me");
				
				// add new properties
				repositoryConnection.add(subject, FOAF.MBOX, valueFactory.createURI("mailto:e0828633@student.tuwien.ac.at"));
				repositoryConnection.add(subject, FOAF.NICK, valueFactory.createLiteral("ckristo"));
				
				// alter an existing statement
				repositoryConnection.remove(subject, FOAF.AGE, null);
				repositoryConnection.add(subject, FOAF.AGE, valueFactory.createLiteral(25));
				
				// NOTE: Sesame doesn't allow to alter existing statements --> remove old and add a new one instead!
				
				// find some existing statements and iterate over them
				RepositoryResult<Statement> statements = repositoryConnection.getStatements(subject, FOAF.MBOX, null, true);
				while (statements.hasNext()) {
					Statement statement = statements.next();
					Value mbox = statement.getObject();
					
					System.out.println(mbox);
				}
				statements.close();
				
				// delete some statements
				repositoryConnection.remove(subject, FOAF.MBOX, null);
				
				// print modified RDF document
				statements = repositoryConnection.getStatements(null, null, null, true);
				Model model = Iterations.addAll(statements, new LinkedHashModel());
				model.setNamespace("rdf", RDF.NAMESPACE);
				model.setNamespace("foaf", FOAF.NAMESPACE);
				Rio.write(model, System.out, RDFFormat.NTRIPLES);
			} catch (RDFParseException | RDFHandlerException | IOException e) { 
				/* error handling */
			} finally {
				repositoryConnection.close();
			}
		} catch (RepositoryException e) { /* error handling */ }
	}
}
