package at.ac.tuwien.big.sw_examples.sesame2_example;

import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.sparql.SPARQLRepository;

/**
 * Example on how to use the OpenRDF Sesame API to query a local repository
 */
public class RemoteSPARQLEndpointExample {
	
	public static void main(String[] args) {
		try {
			// create an empty, in-memory repository
			SPARQLRepository repository = new SPARQLRepository("http://dbpedia.org/sparql");
			repository.initialize();
			
			// obtain connection to repository
			RepositoryConnection repositoryConnection = repository.getConnection();
			
			try {
				TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(QueryLanguage.SPARQL, 
						  "PREFIX dbpedia-owl: <http://dbpedia.org/ontology/> "
						+ "PREFIX dbpprop: <http://dbpedia.org/property/> "
						+ "SELECT ?country ?population "
						+ "WHERE { "
						+ "    ?country a dbpedia-owl:Country . "
						+ "    ?country dbpprop:populationCensus ?population . "
						+ "    FILTER ( isNumeric(?population) && ?population > 50000000 ) "
						+ "} "
						+ "ORDER BY DESC(?population) "
						+ "LIMIT 5");
				
				TupleQueryResult result = tupleQuery.evaluate();
				try {
					while (result.hasNext()) {
						BindingSet bindingSet = result.next();
						
						// get variable bindings
						Resource country = (Resource) bindingSet.getValue("country");
						Literal population = (Literal) bindingSet.getValue("population");
						
						System.out.println("<"+country+"> "+population.getLabel());
					}
				} catch (Exception e) {
					/* error handling */
				} finally {
					result.close();
				}
			} catch (MalformedQueryException | QueryEvaluationException e) {
				/* error handling */
			} finally {
				repositoryConnection.close();
			}
		} catch (RepositoryException e) { /* error handling */ }
	}
}
