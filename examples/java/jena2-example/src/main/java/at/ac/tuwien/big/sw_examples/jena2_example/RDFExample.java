package at.ac.tuwien.big.sw_examples.jena2_example;

import java.io.InputStream;
import java.util.Iterator;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import com.hp.hpl.jena.util.FileManager;

/**
 * Example on how to use the Apache Jena's RDF API
 */
public class RDFExample {

    public static void main(String[] args) {
        // create empty default model
        Model model = ModelFactory.createDefaultModel();

        // read data from RDF document
        InputStream is = FileManager.get().open("foaf.rdf");
        if (is != null) {
            model.read(is, null);
        } else { /* error handling... */ }

        // find the resource that represents me
        Resource resource = model.getResource("http://kindl.io/christoph/foaf.rdf#me");

        // add new properties
        resource.addProperty(FOAF.mbox, model.createResource("mailto:e0828633@student.tuwien.ac.at"));
        resource.addProperty(FOAF.nick, "ckristo");

        // alter an existing statement
        Statement age = resource.getProperty(model.createProperty("http://xmlns.com/foaf/0.1/age"));
        age.changeLiteralObject(25);

	// NOTE: if more than one property of the same type is available,
        //       Resource.getProperty(Property) will deliver an arbitrary one!
        
        // find some existing statements and iterate over them
        Iterator<Statement> iterator = model.listStatements(resource, FOAF.mbox, (RDFNode) null);
        while (iterator.hasNext()) {
            Statement stmt = iterator.next();
            Resource mbox = stmt.getResource();

            // NOTE: Jena doesn't allow to alter the model during iteration!
            
            System.out.println(mbox);
        }

        // delete some statements
        resource.removeAll(FOAF.mbox);

        // print modified RDF document
        model.write(System.out, "N-Triples");

        // cleanup
        model.removeAll();
        model.close();
    }
}
