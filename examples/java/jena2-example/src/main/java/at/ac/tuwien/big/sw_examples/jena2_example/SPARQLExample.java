package at.ac.tuwien.big.sw_examples.jena2_example;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Example on how to use the Apache Jena's ARQ library
 */
public class SPARQLExample {

    public static void main(String[] args) {
        // load external RDF document
        Model model = ModelFactory.createDefaultModel();
        model.read("http://dig.csail.mit.edu/2008/webdav/timbl/foaf.rdf");

        // prepare query
        Query query = QueryFactory.create(
                "PREFIX foaf: <http://xmlns.com/foaf/0.1/> "
                + "SELECT ?person ?name "
                + "WHERE { "
                + "    <http://www.w3.org/People/Berners-Lee/card#i> foaf:knows ?person . "
                + "    OPTIONAL { ?person foaf:name ?name } "
                + "}");

        // execute query
        try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
            ResultSet results = qexec.execSelect();

            // NOTES: 
            // * result set can be traversed only once
            // * result set is no longer available after the query execution is 
            //   closed (i.e. after the try resource block)
            // --> use ResultSetFactory.copyResults(ResultSet)
            
            // iterate over results
            while (results.hasNext()) {
                QuerySolution soln = results.nextSolution();

                // get variable bindings
                Resource person = soln.getResource("person");
                Literal name = soln.getLiteral("name");

                System.out.print((name == null) ? "?" : name);
                System.out.println(" <" + person + ">");
            }
        }
        // AUTO-CLOSE

        // cleanup
        model.removeAll();
        model.close();
    }
}
