package at.ac.tuwien.big.sw_examples.jena2_example;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Example on how to use the Apache Jena's ARQ library
 */
public class RemoteSPARQLEndpointExample {

    public static void main(String[] args) {
        // prepare query
        Query query = QueryFactory.create(
                "PREFIX dbpedia-owl: <http://dbpedia.org/ontology/> "
                + "PREFIX dbpprop: <http://dbpedia.org/property/> "
                + "SELECT ?country ?population "
                + "WHERE { "
                + "    ?country a dbpedia-owl:Country . "
                + "    ?country dbpprop:populationCensus ?population . "
                + "    FILTER ( isNumeric(?population) && ?population > 50000000 ) "
                + "} "
                + "ORDER BY DESC(?population) "
                + "LIMIT 5");

        // execute query against remote SPARQL endpoint
        try (QueryExecution qexec = QueryExecutionFactory.sparqlService("http://dbpedia.org/sparql", query)) {
            ResultSet results = qexec.execSelect();

            // NOTES: 
            // * result set can be traversed only once
            // * result set is no longer available after the query execution is closed (i.e. after the try resource block)
            // --> use ResultSetFactory.copyResults(ResultSet)
            
            // iterate over results
            while (results.hasNext()) {
                QuerySolution soln = results.nextSolution();

                // get variable bindings
                Resource country = soln.getResource("country");
                Literal population = soln.getLiteral("population");

                System.out.println("<" + country + "> " + population.getValue());
            }
        }
        // AUTO-CLOSE
    }
}
