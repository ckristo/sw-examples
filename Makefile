DOKUMENT = main
LATEX_OPTS = -shell-escape

all : $(DOKUMENT).tex sections/sec*.tex references/*.bib
	pdflatex $(LATEX_OPTS) $(DOKUMENT).tex
	bibtex $(DOKUMENT)1
	bibtex $(DOKUMENT)2
	bibtex $(DOKUMENT)3
	# TODO: extend the above to an arbitrary number of bibtex calls for every main<nr>.aux file
	pdflatex $(LATEX_OPTS) $(DOKUMENT).tex
	pdflatex $(LATEX_OPTS) $(DOKUMENT).tex

preview : $(DOKUMENT).tex sections/sec*.tex
	pdflatex $(LATEX_OPTS) $(DOKUMENT).tex

clean : 
	rm -f $(DOKUMENT).tex~
	rm -f $(DOKUMENT).out
	rm -f $(DOKUMENT).toc
	rm -f $(DOKUMENT).bbl
	rm -f $(DOKUMENT).blg
	rm -f $(DOKUMENT).pyg
	rm -f $(DOKUMENT){1,2,3}{.bbl,.blg}
	rm -f *.aux
	rm -f *.log
	rm -f sections/*.aux
	rm -f sections/*.log
	rm -rf _minted-main
clean-all : clean
	rm -f $(DOKUMENT).pdf

.PHONY : all clean
