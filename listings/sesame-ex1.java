try {
  // - create an empty, in-memory repository
  Repository repository = new SailRepository(new MemoryStore());
  repository.initialize();
  
  // - obtain connection to repository
  RepositoryConnection repositoryConnection = repository.getConnection();
  ValueFactory valueFactory = repositoryConnection.getValueFactory();
  
  try {
    // - read data from RDF document
    InputStream is = RepositoryAPI.class.getResourceAsStream("/foaf.rdf");
    repositoryConnection.add(is, "file:foaf.rdf", RDFFormat.RDFXML);
    
    Resource subject = valueFactory.createURI("http://kindl.io/christoph/foaf.rdf#me");
    
    // - add new properties
    repositoryConnection.add(subject, FOAF.MBOX, 
            valueFactory.createURI("mailto:e0828633@student.tuwien.ac.at"));
    repositoryConnection.add(subject, FOAF.NICK, 
            valueFactory.createLiteral("ckristo"));
      
    // - alter an existing statement
    repositoryConnection.remove(subject, FOAF.AGE, null);
    repositoryConnection.add(subject, FOAF.AGE, valueFactory.createLiteral(25));
      
    // - find some existing statements and iterate over them
    RepositoryResult<Statement> statements = 
            repositoryConnection.getStatements(subject, FOAF.MBOX, null, true);
    while (statements.hasNext()) {
      Statement statement = statements.next();
      Value mbox = statement.getObject();
      
      System.out.println(mbox);
    }
    statements.close();
    
    // - delete some statements
    repositoryConnection.remove(subject, FOAF.MBOX, null);
    
    // - print modified RDF document
    statements = repositoryConnection.getStatements(null, null, null, true);
    Model model = Iterations.addAll(statements, new LinkedHashModel());
    model.setNamespace("rdf", RDF.NAMESPACE);
    model.setNamespace("foaf", FOAF.NAMESPACE);
    
    Rio.write(model, System.out, RDFFormat.RDFXML);
  } catch (RDFParseException | IOException | RDFHandlerException e) { 
    /* error handling */
  } finally {
    repositoryConnection.close();
  }
} catch (RepositoryException e) { /* error handling */ }
