try {
  // - create an empty, in-memory repository
  SPARQLRepository repository = new SPARQLRepository("http://dbpedia.org/sparql");
  repository.initialize();
  
  // - obtain connection to repository
  RepositoryConnection repositoryConnection = repository.getConnection();
  
  try {
    TupleQuery tupleQuery = repositoryConnection.prepareTupleQuery(
        QueryLanguage.SPARQL, 
          "PREFIX dbpedia-owl: <http://dbpedia.org/ontology/> "
        + "PREFIX dbpprop: <http://dbpedia.org/property/> "
        + "SELECT ?country ?population "
        + "WHERE { "
        + "    ?country a dbpedia-owl:Country . "
        + "    ?country dbpprop:populationCensus ?population . "
        + "    FILTER ( isNumeric(?population) && ?population > 50000000 ) "
        + "} "
        + "ORDER BY DESC(?population) "
        + "LIMIT 5");
    
    TupleQueryResult result = tupleQuery.evaluate();
    try {
      while (result.hasNext()) {
        BindingSet bindingSet = result.next();
        
        // - get variable bindings
        Resource country = (Resource) bindingSet.getValue("country");
        Literal population = (Literal) bindingSet.getValue("population");
        
        System.out.println("<"+country+"> "+population);
      }
    } catch (Exception e) { e.printStackTrace(); } 
    finally {
      result.close();
    }
  } catch (MalformedQueryException | QueryEvaluationException e) {
    /* error handling */
  } finally {
    repositoryConnection.close();
  }
} catch (RepositoryException e) { /* error handling */ }
