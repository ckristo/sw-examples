
# Sesame #

http://rdf4j.org/sesame/2.7/docs/users.docbook?view

- framework for processing RDF data
- research prototype for an EU research project "On-To-Knowledge" by Aduna -- a dutch company [Programming the SW]
- now developed as open source community project with Aduna as project leed
- commercial support available from Aduna

- triplet stores (in-memory and native store) plus 3rd party solutions
- full SPARQL 1.1 support

- parts of sesame: 
  * Rio = RDF I/O (set of parsers/writers for RDF formats)
  * Sail API = Storage and Inference Layer API
    - a low-level system api for RDF stores and inferencers
	- abstract from storage and inferencer used
	- **relevant mainly for triple store developers**
  * Repository API
    - higher level API that offers developer-oriented methods for handling RDF data
	- functions for loading, querying, extracting, manipulating RDF data
	- local storage and HTTP
  * server solution for offering a SPARQL endpoint
  * custom query language SeRQL

## RDF Model API ##

* defines how the building blocks of RDF (statements, resources, literals, etc.) are represented
* `Statement`: has subject, predicate, object and an optional context
* `Value`: super-type for resource and literal
* `Resource`: represents an RDF resource / uri
* `Literal`: represents RDF literals
* `ValueFactory` for creating resources and literals
* `Model`: collection of statements (extension of Set<Statement>) --> just iterate over this model
   - model.add
   - model.filter
   - two default-implementations for the model: `LinkedHashModel`, `TreeModel` with different performance characteristics
   
### Code examples ###

	// add another RDF statement by simply providing subject, predicate, and object.
	model.add(bob, name, bobsName);
 
	// iterate over every statement in the Model
	for (Statement statement: model) {
	       ...
	}
	
	// iterate over a filtered set of statements
	for (Statement typeStatement: model.filter(null, RDF.TYPE, FOAF.PERSON)) {
	  ...
	}
	
	// filter over the subjects of a filtered set of statements
	for (Resource person: model.filter(null, RDF.TYPE, FOAF.PERSON).subjects()) {
	  // get the name of the person
	  Literal name = model.filter(person, FOAF.NAME, null).objectLiteral(); 
	  ...
	​}

## Repository API ##

* developer-friendly access point to RDF repositories, offering various methods for querying/updating data
* three main types of repositories:
  - SailRepository: access/create local Sesame repositories
  - (( HTTPRepository: acts as proxy for a remote Sesame repository on a Sesame server over HTTP ))
  - SPARQLRepository: acts as proxy for a remote SPARQL endpoint
* local in-memory store:
	```
	Repository repo = new SailRepository(new MemoryStore());
	repo.initialize();
	```
* file-based memory store (give a directory as `File` object to the constructor of `MemoryStore`)
* native store: save data directly on disk; binary format; indices
	```
	File dataDir = new File("/path/to/datadir/");
	Repository repo = new SailRepository(new NativeStore(dataDir));
	repo.initialize();
	```
* repositories with inference support, e.g. RDFS:
	```
	Repository repo = new SailRepository(
	                          new ForwardChainingRDFSInferencer(
	                          new MemoryStore()));
	```
* to perform operations on a repository, request a `RepositoryConnection`
* different query types
  - tuple query :: returns a set of tuples (variable bindings) --> SELECT
  - graph query :: returns a (sub-)graph --> CONSTRUCT, DESCRIBE
  - boolean query :: returns a boolean value --> ASK
* `TupleQueryResult` -> a sequence of `BindingSet` objects, which contains a set of `Bindings`
* `TupleQueryResult.getBindingNames()` delivers the variable names of the bindings
* alternative: register a `TupleQueryResultHandler` which will be invoked with every result entry
* `GraphQueryResult` either iterate over all statements, or use 
	`Model m = QueryResults.asModel(graphQueryResult)`
* prepared statements for queries possible
	```
	TupleQuery keywordQuery = con.prepareTupleQuery(QueryLanguage.SPARQL,
	      "SELECT ?document WHERE { ?document ex:keyword ?keyword . }");
	keywordQuery.setBinding("keyword", factory.createLiteral(keyword));
	TupleQueryResult keywordQueryResult = keywordQuery.evaluate();
	```
* create, retrieve, remove statements from a repository over a repository connection:
  - add: `RepositoryConnection.add(s,p,o)`
  - retrieve: `RepositoryResult<Statement> statements = con.getStatements(alice, null, null, true)` -- last boolean indicates if inferred statements should be included
  - remove: `RepositoryConnection.remove(s,p,o)`
* operate on a set of statements -- e.g. for batch-like update operations
* transactional changes on model (`con.begin()`, `con.commit()`, `con.rollback()`)

### Code examples ###

	// #### add remote RDF data to the repository ####
	
	File file = new File("/path/to/example.rdf");
	String baseURI = "http://example.org/example/local";

	try {
	   RepositoryConnection con = repo.getConnection();
	   try {
	      con.add(file, baseURI, RDFFormat.RDFXML);

	      URL url = new URL("http://example.org/example/remote.rdf");
	      con.add(url, url.toString(), RDFFormat.RDFXML);
	   }
	   finally {
	      con.close();
	   }
	}
	catch (OpenRDFException e) {
	   // handle exception
	}
	catch (java.io.IOEXception e) {
	   // handle io exception
	}

	// #### query a repository with SPARQL ####
	
	try {
	   RepositoryConnection con = repo.getConnection();
	   try {
		  String queryString = "SELECT ?x ?y WHERE { ?x ?p ?y } ";
		  TupleQuery tupleQuery = con.prepareTupleQuery(QueryLanguage.SPARQL, queryString);

		  TupleQueryResult result = tupleQuery.evaluate();
		  try {
	              while (result.hasNext()) {  // iterate over the result
				BindingSet bindingSet = result.next();
				Value valueOfX = bindingSet.getValue("x");
				Value valueOfY = bindingSet.getValue("y");

				// do something interesting with the values here...
	              }
		  }
		  finally {
		      result.close();
		  }
	   }
	   finally {
	      con.close();
	   }
	}
	catch (OpenRDFException e) {
	   // handle exception
	}
	
	// #### retrieve and iterate over statements from respository ####
	RepositoryResult<Statement> statements = con.getStatements(alice, null, null, true); // null -> wildcard

	try {
	   while (statements.hasNext()) {
	      Statement st = statements.next();

	      ... // do something with the statement
	   }
	}
	finally {
	   statements.close(); // make sure the result object is closed properly
	}
	
### RIO API ###

* pass the document (as URL, File, InputStream, Reader) to the repository -- will be stored
* no intermediate storage of data -> parsers from the Rio API
* parser will work with a set of listeners they report to (`ParseErrorListener`, `ParseLocationListener`, `RDFHandler`)

#### Code examples ####

	// #### parse a remote Turtle document ####
	java.net.URL documentUrl = new URL(“http://example.org/example.ttl”);
	InputStream inputStream = documentUrl.openStream();
	
	RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
	
	org.openrdf.model.Model myGraph = new org.openrdf.model.impl.LinkedHashModel();
	rdfParser.setRDFHandler(new StatementCollector(myGraph));
	
	try {
	   rdfParser.parse(inputStream, documentURL.toString());
	}
	catch (IOException e) {
	  // handle IO problems (e.g. the file could not be read)
	}
	catch (RDFParseException e) {
	  // handle unrecoverable parse error
	}
	catch (RDFHandlerException e) {
	  // handle a problem encountered by the RDFHandler
	}
	
	// #### helper method since Sesame 2.7.1 ####
	RDFFormat format = Rio.getParserFormatForMIMEType("application/rdf+json");
	// or to use a file extension instead
	if(format == null) {
	    format = Rio.getParserFormatForFileName("test.rj");
	}

	Model results = Rio.parse(inputStream, documentURL.toString(), format);
	
	// #### write to a RDF file ####
	Model myGraph; // a collection of several RDF statements
	FileOutputStream out = new FileOutputStream("/path/to/file.rdf");
	RDFWriter writer = Rio.createWriter(RDFFormat.RDFXML, out);
	try {
	  writer.startRDF();
	  for (Statement st: myGraph) {
		 writer.handleStatement(st);
	  }
	  writer.endRDF();
	}
	catch (RDFHandlerException e) {
	 // oh no, do something!
	}
	
	// #### helper method since Sesame 2.7.1 ####
	Model myGraph; // a collection of 
	FileOutputStream out = new FileOutputStream("/path/to/file.rdf")
	RDFFormat format = Rio.getWriterFormatForMIMEType("application/rdf+xml");

	Rio.write(myGraph, out, format);
	
	// #### parse a file from one input format to another ####
	// open our input document
	java.net.URL documentUrl = new URL(“http://example.org/example.ttl”);
	InputStream inputStream = documentUrl.openStream();

	// create a parser for Turtle and a writer for RDF/XML 
	RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
	RDFWriter rdfWriter = Rio.createWriter(RDFFormat.RDFXML, 
	                           new FileOutputStream("/path/to/example-output.rdf");

	// link our parser to our writer...
	rdfParser.setRDFHandler(rdfWriter);

	// ...and start the conversion!
	try {
	   rdfParser.parse(inputStream, documentURL.toString());
	}
	catch (IOException e) {
	  // handle IO problems (e.g. the file could not be read)
	}
	catch (RDFParseException e) {
	  // handle unrecoverable parse error
	}
	catch (RDFHandlerException e) {
	  // handle a problem encountered by the RDFHandler
	}
	
	// #### guess format ####
	RDFFormat format = Rio.getParserFormatForFileName(documentURL.toString());
	RDFFormat format = Rio.getParserFormatForMIMEType(contentType);
	
	RDFParser rdfParser = Rio.createParser(format);
	
