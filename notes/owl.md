# OWL Web Ontology Language #

## Introduction ##
* most popular language to use when creating ontologies
* purpose of OWL = purpose of RDFS (define ontologies incl. classes, properties and their relationship for a specific application domain)
* OWL provides capabilities to express more complex and richer relationships => applications with stronger reasoning ability
* OWL = RDFS + new constructs for better expressiveness
* OWL is built up on RDFS => all RDFS terms can be used in OWL documents
* OWL is computational logic-based (=> make implicit knowledge explicit, verify consistency)
* OWL 1 vs. OWL 2 (corrects shortcomings of OWL 1)
* OWL = collection of terms we can use to define classes and properties for spec. application domains

	http://www.w3.org/2002/07/owl# => owl:

## Basics ##

**axiom**: basic statement that OWL ontology has; represents a basic piece of knowledge (e.g. class A is a sub-class of class B)
* OWL ontology = collection of axioms; asserts that all its axioms are true
* each axiom (as a statement) involves some class, property or individual; classes, properties and individuals can be viewed as atomic constituents of axioms (called entities)
* individual entity called *object*, class entity called *category* and property entity called *relation*
* combinations of different class entities and/or property entities that form new class entities and property entities (called *expressions*)

## Syntax Forms ##

OWL spec provides several syntaxes for persisting, sharing and editing ontologies. OWL itself is not defined by using a particular concrete syntax, but rather by a high-level structural specification.

### Functional-Style syntax ###

* used to translate structural specification to various other syntaxes (often used by OWL tool designers).
* not intended to be used as exchange syntax

### RDF/XML syntax ###

* mandatory to be supported by all OWL tools

### Manchester syntax ###

* provides a textual based representation of OWL ontologies that is easy to read and write

### OWL/XML ###

* RDF/XML is not easy to work with; difficult to use with XML tools like Xpath and XSLT
* in order to take advantages of existing XML tools, a more regular and simple XML format was needed
* conforms an XMLSchema4

## OWL 1 ##

OWL offers much greater expressiveness for defining classes than RDFS
`owl:Thing` is the root of all classes in OWL
`owl:Class` to define new classes

	<owl:Class rdf:about="http://www.liyangyu.com/camera#Camera"> </owl:Class>

`owl:Restriction` .. term describe an anonymous class which is defined by adding some restrictions on some property. Has two parts:
* 1.Part: specifies the property to which the restriction applies `owl:onProperty`
* 2.Part: about the property contraint itself, what is the constraint
  * *value constraints* : puts contraints on the range of the property
  * *cardinality contraints* : puts contraints on the number of values a property can take

**Value contraints:**

`owl:allValuesFrom` all values for the restricted property has to come from specified class or data range, but no properties are possible after all

`owl:someValuesFrom` .. at least one value for restricted property has to come from specified class, thus property has to appear at least once

	<owl:Class rdf:about="http://www.liyangyu.com/camera#ExpensiveDSLR">
		<rdfs:subClassOf rdfs:resource="http://www.liyangyu.com/camera#DSLR"/>
		<rdfs:subClassOf>
			<owl:Restrction>
				<owl:onProperty rdf:resource="http://www.liyangyu.com/camera#owned_by"/>
				<owl:allValuesFrom <!-- or someValuesFrom --> rdf:resource="http://www.liyangyu.com/camera#Professional"/>
			</owl:Restriction>
		</rdfs:subClassOf>
	</owl:Class>

`owl:hasValue` .. at least one value of the property is equal to a specified value; more specific than `owl:someValuesFrom` because it requires a particular instance.
==> allow to reason the specific class type for property values or subjects

	<owl:Class rdf:about="http://www.liyangyu.com/camera#ExpensiveDSLR">
		<rdfs:subClassOf rdf:resource="#DSLR"/>
		<rdfs:subClassOf>
			<owl:Restriction>
				<owl:onProperty rdf:resource="#cost"/>
				<owl:hasValue rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
					expensive
				</owl:hasValue>
			</owl:Restriction>
		</rdfs:subClassOf>
	</owl:Class>

**Cardinality constraints:**

`owl:cardinality` allows to specify how often the property can be specified

	<owl:Class rdf:about="http://www.liyangyu.com/camera#Digital">
		<rdfs:subClassOf rdf:resource="#Camera"/>
		<rdfs:subClassOf>
			<owl:Restriction>
				<owl:onProperty rdf:resource="#effectivePixel"/>
				<owl:cardinality rdf:datatype="http://www.w3.org/2001/XMLSchema#nonNegativeInteger">
					1
				</owl:cardinality>
			</owl:Restriction>
		</rdfs:subClassOf>
	</owl:Class>

==> can have multiple effectivePixel values, but these values must all be equal

`owl:minCardinality` / `owl:maxCardinality` restrict the number of min. / max. occurrence of a property

**`owl:sameAs`** => two URIs describe exactly the same thing (aliases)

**Defining classes: set operators**

`owl:intersectionOf`

	<owl:Class rdf:about="http://www.liyangyu.com/camera#ExpensiveDSLR">
		<owl:intersectionOf rdf:parseType="Collection">
			<owl:Class rdf:about="#DSLR"/>
			<owl:Restriction>
				<owl:onProperty rdf:resource="#cost"/>
				<owl:hasValue rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
					expensive
				</owl:hasValue>
			</owl:Restriction>
		</owl:intersectionOf>
	</owl:Class>

^= Multiple rdfs:subClassOf = subset of the intersection of all the classes specified

`owl:unionOf`

	<owl:Class rdf:about="http://www.liyangyu.com/camera#CameraCollection">
		<owl:unionOf rdf:parseType="Collection">
			<owl:Class rdf:about="#Digital"/>
			<owl:Class rdf:about="#Film"/>
		</owl:unionOf>
	</owl:Class>

`owl:complementOf`

	<owl:Class rdf:about="http://www.liyangyu.com/camera#Amateur">
		<owl:intersectionOf rdf:parseType="Collection">
			<owl:Class rdf:about="http://xmlns.com/foaf/0.1/Person"/>
			<owl:Class>
				<owl:complementOf rdf:resource="#Professional"/>
			</owl:Class>
		</owl:intersectionOf>
	</owl:Class>

Enumerating all instances of a class with `owl:oneOf` (more efficient way in some cases)

	<owl:Class rdf:about="http://www.liyangyu.com/camera#ExpensiveDSLR">
		<rdfs:subClassOf rdf:resource="#DSLR"/>
		<owl:oneOf rdf:parseType="Collection">
			<myCamera:DSLR rdf:about="http://dbpedia.org/resource/Nikon_D3"/>
			<myCamera:DSLR rdf:about="http://dbpedia.org/resource/Canon_EOS-1D"/>
		</owl:oneOf>
	</owl:Class>

also fine to use `owl:Thing` instead of `myCamera:DSLR`

`owl:equivalentClass`

	<owl:Class rdf:about="http://www.liyangyu.com/camera#DigitalSLR">
		<owl:equivalentClass rdf:resource="#DSLR"/>
	</owl:Class>

allows to declare that two classes are equivalent (mostly used across different ontologies)

	<owl:Class rdf:about="http://www.liyangyu.com/camera#DSLR">
		<rdfs:subClassOf rdf:resource="#Digital"/>
		<owl:equivalentCass rdf:resource="http://www.example.com#DigitalSingleLensReflex"/>
	</owl:Class>

`owl:disjointWith`

	<owl:Class rdf:about="http://www.liyangyu.com/camera#DSLR">
		<rdfs:subClassOf rdf:resource="#Digital"/>
		<owl:equivalentCass rdf:resource="http://www.example.com#DigitalSingleLensReflex"/>
		<owl:disjointWith rdf:resource="#PointAndShoot"/>
	</owl:Class>

* A disjointWith B ==> instance of A will never be an instance of B
* symmetric: A disjointWith B ==> B disjointWith A

**Defining properties**

`owl:ObjectProperty` used to connect a resource to another resource
`owl:DatatypeProperty` used to connect a resource to an rdfs:Literal (untyped) or an XML Schema built-in datatype (typed) value.

* **Symmetric properties**

A property B ==> B property A

	<owl:ObjectProperty rdf:about="http://www.liyangyu.com/camera#friend_with">
		<rdf:type rdf:resource="http://www.w3.org/2002/07/owl#SymmetricProperty"/>
		<rdfs:domain rdf:resource="#Photographer"/>
		<rdfs:range rdf:resource="#Photographer"/>
	</owl:ObjectProperty>

==> owl:SummetricProperty

* **Transitive properties**

R1 property R2 && R2 property R3 ==> R1 property R3

	<owl:ObjectProperty rdf:about="http://www.liyangyu.com/camera#betterQPRatio">
		<rdf:type rdf:resource="http://www.w3.org/2002/07/owl#TransitiveProperty"/>
		<rdfs:domain rdf:resource="#Camera"/>
		<rdfs:range rdf:resource="#Camera"/>
	</owl:ObjectProperty>

* **Functional properties**

For every given instance, there's at most one value for the property. (many-to-one relation)

	<owl:ObjectProperty rdf:about="http://www.liyangyu.com/camera#manufactured_by">
		<rdf:type rdf:resource="http://www.w3.org/2002/07/owl#FunctionalProperty"/>
		<rdfs:domain rdf:resource="#Camera"/>
	</owl:ObjectProperty>

^= cardinality=1

* **Inverse properties**

R1 property R2 ==> R2 inverse of property R1

	<owl:ObjectProperty rdf:about="http://www.liyangyu.com/camera#owned_by">
		<rdfs:domain rdf:resource="#DSLR"/>
		<rdfs:range rdf:resource="#Photographer"/>
	</owl:ObjectProperty>
	<owl:ObjectProperty rdf:about="http://www.liyangyu.com/camera#own">
		<owl:inverseOf rdf:resource="#owned_by"/>
		<rdfs:domain rdf:resource="#Photographer"/>
		<rdfs:range rdf:resource="#DSLR"/>
	</owl:ObjectProperty> 

`owl:inverseOf`

* **Inverse Functional properties**

Opposite of functional property: for a given range value, the domain value must be unique

	<owl:DatatypeProperty rdf:about="http://www.liyangyu.com/camera#reviewerID">
		<rdf:type rdf:resource="http://www.w3.org/2002/07/owl#InverseFunctionalProperty"/>
		<rdfs:domain rdf:resource="#Photographer"/>
		<rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#string"/>
	</owl:DatatypeProperty>
	<rdfs:Datatype rdf:about="http://www.w3.org/2001/XMLSchema#string"/>

(is a functional property too -- like most IDs)

## Reasoning Power (Examples) ##

TODO

## OWL 2 ##

new features:
1. syntactic sugar to make common statements easier to construct
2. new constructs to improve expressiveness (reflexive property, irreflexive property, asymmetric property, qualified cardinality constraints, property chains and keys)
3. extended support for datatypes (more built-in data types, allows to define custom datatypes)
4. simple metamodeling capabilities (plunning) and extended annotation capabilities
5. sub-langes: profiles OWL 2 EL, OWL 2 QL, OWL 2 PL; offer different level of tradeoff between expressiveness and efficiency

### Common patterns ###

* disjointness: OWL 1 `owl:disjointWith` can only be used to express that two classes are disjoint, it has to be used on _every possible class pair_. => `owl:AllDisjointClasses`

	<owl:AllDisjointClasses>
		<owl:members rdf:parseType="Collection">
			<owl:Class rdf:about="&myCamera;DSLR"/>
			<owl:Class rdf:about="&myCamera;PointAndShoot"/>
			<owl:Class rdf:about="&myCamera;Film"/>
	 	</owl:members>
	</owl:AllDisjointClasses>
	
* `owl:disjointUnionOf` express the fact that a given class collection is a union of pairwise disjoint classes (OWL 1: `owl:unionOf` + multiple `owl:disjointWith`)

	<owl:Class rdf:about="&myCamera;CameraCollection"> 
		<owl:disjointUnionOf>
			<owl:members rdf:parseType="Collection">
				<owl:Class rdf:about="&myCamera;DSLR"/>
				<owl:Class rdf:about="&myCamera;PointAndShoot"/>
				<owl:Class rdf:about="&myCamera;Film"/>
			</owl:members>
		</owl:disjointUnionOf>
	</owl:Class>
	
* negative fact assertions: A !property B => `owl:NegativeObjectPropertyAssertion`, `owl:NegativeDataPropertyAssertion`

	<myCamera:Photographer rdf:about="http://liyangu.com#liyang"/>
	<myCamera:DSLR rdf:about="http://dbpedia.org/resource/Canon_EOS_7D"/>
	<owl:NegativeObjectPropertyAssertion>
		<owl:sourceIndividual rdf:resource="http://liyangyu.com#liyang"/>
		<owl:assertionProperty rdf:resource="&myCamera;own"/>
		<owl:taretIndividual rdf:resource="http://dbpedia.org/resource/Canon_EOS_7D"/>
	</owl:NegativeObjectPropertyAssertion>
	
	<owl:NegativeDataPropertyAssertion>
		<owl:sourceIndividual rdf:resource="http://dbpedia.org/resource/Nikon_D300"/>
		<owl:assertionProperty rdf:resource="&myCamera;effectivePixel"
		<owl:targetValue rdf:datatype="http://www.liyangyu.com/camera#MegaPixel">
			10
		</owl:targetValue>
	</owl:NegativeDataPropertyAssertion>
	
## Improved expressiveness ##

* property self-restriction

OWL 1 doesn't allow that a class is related o itself by some property. => `owl:hasSelf`

	<owl:Class rdf:about="&myExample;Thread">
		<owl:equivalentClass>
			<owl:Restriction>
				<owl:onProperty rdf:resource="&myExample;create"/>
				<owl:hasSelf rdf:datatype="&xsd;boolean">true</owl:hasSelf>
		</owl:Restriction>
	  </owl:equivalentClass>
	</owl:Class>

* qualified cardinality restrictions: not only number of specified properties, but also data range of instances counted is important (e.g. marriage: 2 persons; 1m, 1f) => `owl:minQualifiedCardinality` (at-least), `owl:maxQualifiedCardinality` (at-most), `owl:qualifiedCardinality` (exact)

	<owl:Class rdf:about="&myCamera;Professional"> 
		<rdfs:subClassOf rdf:resource="&myCamera;Photographer"/>
		<rdfs:subClassOf>
			<owl:Restriction>
				<owl:minQualifiedCardinality rdf:datatype="&xsd;nonNegativeInteger">
					1
				</owl:minQualifiedCardinality>
				<owl:onProperty rdf:resource="&myCamera;own"/>
				<owl:onClass rdf:resource="&myCamera;ExpensiveDSLR"/>
			</owl:Restriction>
		</rdfs:subClassOf>
	</owl:Class>

=> owl:onClass to specify the type of the instance counted

* property characteristics **reflexive** (every resource relates to itself), **irreflexive** (no resource can be related to itself), *asymmetric** (A property B =/=> B property A

	<owl:ReflexiveProperty rdf:about="http://example.org/example1#hasRelative"/>
	
	<owl:IrreflexiveProperty rdf:about="http://example.org/example1#hasParent"/>

	<owl:AsymmetricProperty rdf:about="&myCamera;owned_by">
		<rdfs:domain rdf:resource="&myCamera;DSLR"/>
		<rdfs:range rdf:resource="&myCamera;Photographer"/>
	</owl:AsymmetricProperty>

* **Disjoint properties** (= no two individual resources can be connected by both properties)
  
  - `owl:propertyDisjointWith`: used to specify that 2 properties are mutually exclusive; can be used for datatype and object properties
  
	<owl:AsymmetricProperty rdf:about="&myCamera;owned_by">
		<owl:propertyDisjointWith rdf:resource="&myCamera;own"/> 
		<rdfs:domain rdf:resource="&myCamera;DSLR"/>
		<rdfs:range rdf:resource="&myCamera;Photographer"/>
	</owl:AsymmetricProperty>
  
  - `owl:AllDisjointProperties`: group of object properties are pair-wise disjoint:
  
	<owl:AllDisjointProperties>
		<owl:members rdf:parseType="Collection">
			<owl:ObjectProperty rdf:about="&example;property1"/>
			<owl:ObjectProperty rdf:about="&example;property2"/>
			<owl:ObjectProperty rdf:about="&example;property3"/>
		</owl:members>
	</owl:AllDisjointProperties>
	
* **Property Chains** define new properties by using a property chain (e.g. `ex:hasUncle` => `ex:hasParent` -> `ex:hasBrother`) -- only used on object properties

	<rdf:Description rdf:about="&example;hasUncle">
		<owl:propertyChainAxiom rdf:parseType="Collection">
			<owl:ObjectProperty rdf:about="&example;hasParent"/>
			<owl:ObjectProperty rdf:about="&example;hasBrother"/>
		</owl:propertyChainAxiom>
	</rdf:Description>

* **Keys** `owl:hasKey` can be used to state that each named instance of a given class is uniquely identified by a property or a set of properties (which can be data or object properties)

	<owl:Class rdf:about="&myCamera;Photographer">
		<owl:intersectionOf rdf:parseType="Collection">
			<owl:Class rdf:about="http://xmlns.com/foaf/0.1/Person"/>
			<owl:Class>
				<owl:hasKey rdf:parseType="Collection">
					<owl:DatatypeProperty rdf:about="&myCamera;reviewerID"/>
				</owl:hasKey>
			</owl:Class>
		</owl:intersectionOf>
	</owl:Class>

* **Extended support for Datatypes**
  
  - wider range of supported data types needed
    - XSD data types
    - `owl:real`, `owl:rational`
  
  - capability of adding constraints & capability of creating custom data-types
    
    - restrictions with _facets_ from XSD
      - for numeric datatypes (`xsd:minInclusive`..`xsd:maxExclusive`)
      - `xsd:length`, `xsd:minLength`, `xsd:maxLength` for string-based data types
    
    - create custom data-types (`owl:onDataType`, `owl:withRestrictions`)

	<rdfs:Datatype rdf:about="&example;AdultAge">
		<owl:onDatatype rdf:resource="&xsd;integer"/>
		<owl:withRestrictions rdf:parseType="Collection">
			<rdf:Description>
				<xsd:minInclusive rdf:datatype="&xsd;integer">18</xsd:minInclusive>
			</rdf:Description>
		</owl:withRestrictions>
	</rdfs:Datatype>

    - data range combinations to construct new datatypes with `owl:datatypeComplementOf`, `owl:intersectionOf`, `owl:unionOf`
    
    <rdfs:Datatype rdf:about="&example;MinorAge">
    	<owl:equivalentClass>
    		<owl:intersectionOf rdf:parseType="Collection">
    			<rdfs:Datatype rdf:about="&example;PersonAge"/>
    			<rdfs:Datatype>
    				<owl:datatypeComplementOf rdf:resource="&example;AdultAge"/>
    			</rdfs:Datatype>
    		</owl:intersectionOf>
    	</owl:equivalentClass>
    </rdfs:Datatype>

* **Punning** : 
  - OWL 1 DL disallowed to use the same IRI for both a class and entities of a class
  - OWL 2 relaxed this requirement: same IRI can be used for entities of different kinds ==> Punning; 
  - Reasoning engine will interpret those objects different independant objects
  - OWL 2 restrictions: 
    - 1 IRI cannot denote both a datatype property and an object property
    - 1 IRI cannot be used for both a class and a datatype
  - useful to state facts about classes and properties themselves (meta-modeling)
* **Metamodeling** / **Annotations**: provide ways to associate information with classes and properties
  - metamodeling for information attached to entities that should be considered as part of the domain
  - annotation for information that should not be considered as part of the domain and should not contribute to logical consequences of underlying ontology. e.g. `owl:versionInfo`, `rdfs:label`, `rfds:commment`, custom annotations with `owl:AnnotationProperty`
  - OWL 2 allows to put annoations on ontologies, entities (classes, properties, individuals), anonymous individuals, axioms, on annotations themselves

* **Entity declarations**: declare entities + its type (class, datatype property, object property, datatype, annotation property, individual) before using them => check for errors / consistency => quality of the ontology; e.g.

	<owl:Class rdf:about="&myCamera;Camera"/>
	<!-- before define myCamera:camera -->
	
	<!-- properties -->
	<rdfs:Datatype rdf:about="&myCamera;MegaPixel"/>
	<owl:ObjectProperty rdf:about="&myCamera;own"/>
	<owl:AnnotationProperty rdf:about="http://www.purl.org/metadata/dublin-core#date"/>
	
	<!-- individuals -->
	<owl:NamedIndividual rdf:about="http://dbpedia.org/resource/Nikon_D300"/>
	
* **Top/Bottom properties** (+ to `owl:Thing`, `owl:Nothing` in OWL 1)

* **Imports and Versioning**
  - quite important aspects of ontology management
  - _ontology header_ with ontology name
  
	<owl:Ontology rdf:about=""><!-- defines the name; "" = xml:base -->
		<owl:versionInfo>v.10</owl:versionInfo><!-- in OWL 1 -->
		<rdfs:comment>our camera ontology</rdfs:comment>
	</owl:Ontology>
	
  - `owl:imports` : range and domain = owl:Ontology, name and location of ontology must be equal in OWL 1
  - `owl:versionIRI` replaces `owl:versionInfo` in OWL 2, used as storage location, latest version under ontology IRI
  
	<owl:Ontology rdf:about="">
		<owl:versionIRI>http://www.liyangyu.com/camera/v1</owl:versionIRI>
		<rdfs:comment>our camera ontology</rdfs:comment>
	</owl:Ontology>
	
  - -> `owl:imports` using specific version possible 
  
  - -> "importing by location" rule in OWL2
  
  - `owl:imports` is transitive: A imports B imports C ==> A imports B,C
  - declare a namespace for imported ontology for shorthand of imported ontology's terms

* **Terms for instance documents**

  - `owl:sameAs` (can also be used to state that two classes denote the same real world concept like `owl_equivalentClass`)
  
  	<rdf:Description rdf:about="http://www.liyangyu.com/camera#Nikon_D300">
  		<rdf:type rdf:resource="http://www.liyangyu.com/camera#DSLR"/>
  		<owl:sameAs rdf:resource="http://dbpedia.org/resource/Nikon_D300"/>
  	</rdf:Description>

  - `owl:differentFrom` (opposite of sameAs) and `owl:AllDifferent` + `owl:distinctMemebers` property
  
## OWL 1 Profiles ##

* tradeoff expressiveness vs. efficiency of the reasoning process => richer language = more complex and time-consuming reasoning

* some of OWLs constructs are very expensive, may even lead to uncontrollable computational complexities ==> different subsets of OWL for different levels of tradeoff

* two semantic models: _direct model-theoretic semantics_ and _RDF-based semantics_; OWL was designed to use a notational variant of description logic (DL), but it gained importance to be semantically compatible with existing Semantic Web Standards such as RDF, RFDS. 
==> 2 coexisting semantics for OWL 1: _OWL 1 DL_ and _OWL 1 Full_

### 3 Faces of OWL 1 ###

* designed for maximum expressiveness together with guaranteed computational completeness and decidability (all computations will be finished in finite time)

* OWL 1 DL supports all OWL 1 language constructs _but under certain constraints_

* OWL 1 Full provides max. expressiveness. no restrictions on the usage of built-in OWL 1 vocabulary => reasoning can be undecidable

* OWL 1 DL has a high worst-case computational complexity => fragement of OWL 1 DL called _OWL 1 Lite_

==> 3 faces of OWL 1: OWL 1 Lite, OWL 1 DL, OWL 1 Full

* OWL 1 Lite \subset OWL 1 DL \subset OWL 1 Full => every OWL 1 Lite conclusion is a valid OWL 1 DL conclusion, is a valid OWL 1 Full conclusion

#### OWL 1 Full ####

* Every construct covered above is available to the ontology dev.
* combine these constructs with RDF and RDF Schema definitions -- mixing RDFS and OWL definitions
* Any legal RDF document is a legal OWL 1 Full document

#### OWL 1 DL ####

OWL 1 Full but several restrictions must be complied:

* no arbitrary combinations: a resource can be only a class, a datatype, a datatype property, an object property, an instance, a data value, and not more than these. => class cannot be a member of another class => no punning

* restrictions on functional and inverse functional properties: can only be used with object properties, not datatype properties.

* restrictions on transitive properties: `owl:cardinality` may not be used with transitive properties or their sub-properties (because they are transitive implicitly)

* restrictions on `owl:imports`: OWL 1 DL ontologies importing OWL 1 Full ontologies won't be qualified as OWL 1 DL ontologies

(restrictions on annotations:)

* object properties, datatype properties, annotation properties, and ontology properties must be mutually disjoint (a property cannot be a datatype property and an annotation property at the same time)

* annotation properties must not be used in property axioms (i.e. no sub-properties or domain/range constraints for annotation properties can be defined)

* Annotation properties must be explicitly declared

* the object of an annotation property must be either a literal, an URI reference or an individual, nothing else

#### OWL 1 Lite ####

subset of OWL 1 DL with following main restrictions:

* `owl:hasValue`, `owl:disjointWith`, `owl:unionOf`, `owl:complementOf`, `owl:oneOf` are not allowed

* more restricted cardinality constraints: only `ow:ardinality` can be used, and only with value {0,1}

* `owl:equivalentClass` cannot be used to relate anonymous classes, only to connect class identifiers

## OWL 2 Profiles ##

* again: _Direct-Model-Theoretic Semantics_ (OWL 2 DL) vs. _RDF-based Semantics_ (OWL 2 Full)

* OWL 2 DL is a syntactically restricted version of OWL 2 Full -> an OWL 2 DL reasoner may decide "yes"/"no" to any inference request, OWL 2 Full can be undecidable

* annotations have no formal meaning in OWL 2 DL (they may be used to draw extra inferences in OWL 2 Full)

* OWL 2 defines more _profiles_: OWL 2 EL, OWL 2 QL, OWL 2 RL -> each one of these profiles has its own limitations regarding expressiveness

### OWL 2 EL ###

* designed for very large ontologies

* complexity of reasoning support is know to be worst-case polynomial (PTime-complete algorithms)

* main supported features:
  - allows `owl:someValuesFrom` to be used with class expressions or data range
  - allows `owl:hasValue` to be used with individuals or literals
  - allows usage of self-restrictions `owl:hasSelf`
  - property domains, class/property hierarchies, class intersections, disjoint classes, property chains and keys are fully supported

* NOT supported features:
  - `owl:allValuesFrom` on both class expression and data range
  - none of the cardinality restrictions
  - `owl:unionOf` and `owl_complementOf` are not supported
  - disjoint properties are not supported
  - irreflexive, inverse, functional, inverse functional, symmetric and asymmetric object properties
  - datatypes `xsd:double`, `xsd:float`, `xsd:nonPositiveInteger`, ...

### OWL 2 QL ###

* designed for applications involving classical databases that need to work with OWL ontolgoies

* interoperability of OWL with database technology is main concern

* most important reasoning task: answer queries against large volumes of instance data

* guaranteed polynomial time performance => limited expressive Power

* key features of ER and UML diagrams, can be used as high-level database schema language

* main features:
  - allow `owl:someValuesFrom` with restrictions (see below)
  - property domains and ranges, property hierarchies, disjoint classes or equivalence classes (only for sub-class-type expressions), symmetric, reflexive, irreflexive, asymmetric, and inverse properties supported

* NOT supported features:
  - `owl:someValuesFrom` not supported when used on class expressions or a data range in the sub-cass position
  - `owl:allValuesFrom` is not supported (on class expressions and data range)
  - `owl:hasValue` not supported when used on individuals or a literal
  - `owl:hasKey`, `owl:hasSelf`, `owl:unionOf`, `owl:oneOf` are not supported
  - no cardinality restrictions are supported
  - propert inclusions involving property chains
  - transitive, functional and inverse functional properties
  - datatypes `xsd:double`, `xsd:float`, `xsd:nonPositiveInteger`, ...
  
### OWL 2 RL ###

* designed for applications require scalable reasoning without sacrificing too much expressive power

* guaranteed polynomial time performance -> achieved by restricting the use of constructs to certain syntactic positions, e.g. `owl:subClassOf` requires usage patterns that must be followed by the sub-class and super-class expressions

* supported features: property hierarchies, disjointness, inverse, symmetric, asymmetric and transitive properties, property chains, functional and inverse functional properties, irreflexive property are fully supported

* NOT supported features (in addition to restrictions explicitly defined in OWL 2 RL)
  - property domains and ranges only for sub-class-type expressions
  - disjoint unions of classes and reflexive object properties
  - `owl:real` and `owl:rational` datatypes
