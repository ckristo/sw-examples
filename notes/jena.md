# Jena #

## RDF API ##

### Functionalities ###

important Java classes:
* `Resource` represents an RDF resource
* `Statement` represents an RDF statement
* `RDFNode` common superclass for `Resource` and `Literal`
* `Model`: 
   - class that represents an RDF graph
   - offers basic interaction with data
   - a set (in math. sence) of statements
   - set namespace prefixes with `Model.setNsPrefix("ns", uri)`
   - offers `Model.read(inputStream, baseNsUri)` to read RDF data from a file
   - offers `Model.write(outputStream, format) to write RDF data to file in a specific format (e.g. "RDF/XML", "RDF/XML-ABBREV", "N-TRIPLES")
   - offers some methods for getting data from the graph
     * `Resource r = Model.getResource(uri)`
	 * `Statement st = Resource.getProperty(uri)` -- delivers the whole statement! NOTE: if multiple properties are present, delivers one arbitrarily!
	 * `Resource r = Resource.getProperty(uri).getResource()` -- returns the object as resource
	 * `String s = Resource.getProperty(uri).getString()` -- returns the object as string literal
   - querying the model
     * `StmtIterator iter = Resource.listProperties(uri)`
	 * `Model.listStatements()`, `Model.listSubjects()`, `Model.listSubjectsWithProperty(Property prop, RDFNode obj)`, `Model.listStatements(selector)` -- all return StmtIterator
   - operations on models
     * 3 operations --> common set operations union, intersection and difference
	 * `model1.union(model2)`, `model1.intersection(model2)`, `model1.difference(model2)`
   - literals
     * `model.createLiteral(string, lang)`, `model.createLiteral(string, wellFormed?)` -- only untyped literals
	 * `model.createTypedLiteral()` for typed literals

### Code examples ###

	// create statements and add them to the model
	Resource johnSmith
	  = model.createResource(personURI)
	         .addProperty(VCARD.FN, fullName)
	         .addProperty(VCARD.N,
	                      model.createResource()
	                           .addProperty(VCARD.Given, givenName)
	                           .addProperty(VCARD.Family, familyName));
	
## SPARQL ##

### SELECT ###

* important classes:
  - `Query` -- class representing an application query
  - `QueryExecution` -- represents one execution of a query
  - `QuerySolution` -- a single query solution
  - `ResultSet` -- all query solutions, iterator interface, auto-closable
  - `ResultSetFormatter` -- format a query result set using various formats (into a model object, or as text using a serialization format)
* Select: ResultSet is iterator, can be used to traverse result set only once -- not valid after QueryExecution is closed! --> materialize result set; can then be used to alter the data also --> delivers a ResultSetRewindable iterator
* models returned by execConstruct and execDescribe are valid after QueryExecution is closed!
* get variables of solution -- handle with Jena's RDF API
* query remote SPARQL endpoint: `QueryExecutionFactory.sparqlService`

% TODO: prepared statements with QuerySolutionMap

#### Code examples ####

	Model model = ... ;
	String queryString = " .... " ;
	Query query = QueryFactory.create(queryString) ;
	try (QueryExecution qexec = QueryExecutionFactory.create(query, model))
	{
	  ResultSet results = qexec.execSelect() ;
	  for ( ; results.hasNext() ; )
	  {
	    QuerySolution soln = results.nextSolution() ;
	    RDFNode x = soln.get("varName") ;       // Get a result variable by name.
	    Resource r = soln.getResource("VarR") ; // Get a result variable - must be a resource
	    Literal l = soln.getLiteral("VarL") ;   // Get a result variable - must be a literal
	  }
	}
	
	// step of query creation and execution in one step:
	Model model = ... ;
	String queryString = " .... " ;
	try (QueryExecution qexec = QueryExecutionFactory.create(queryString, model))
	{
	  ResultSet results = qexec.execSelect() ;
	  //...
	}
	
	// materialize result set for outside use
	try (QueryExecution qexec = QueryExecutionFactory.create(queryString, model)) {
	    ResultSet results = qexec.execSelect() ;
	    results = ResultSetFactory.copyResults(results) ;
	    return results ;    // Passes the result set out of the try-resources
	}
	
	// get variable and read with RDF API
	for ( ; results.hasNext() ; )
	{
	    // Access variables: soln.get("x") ;
	    RDFNode n = soln.get("x") ; // "x" is a variable in the query
	    // If you need to test the thing returned
	    if ( n.isLiteral() )
	        ((Literal)n).getLexicalForm() ;
	    if ( n.isResource() )
	    {
	       Resource r = (Resource)n ;
	        if ( ! r.isAnon() )
	        {
	          ... r.getURI() ...
	        }
	    }
	}


### CONSTRUCT / DESCRIBE ###

* simply returns an RDF graph / Jena Model object

#### Code examples ####

	// construct query
	Query query = QueryFactory.create(queryString) ;
	QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
	Model resultModel = qexec.execConstruct() ;
	qexec.close() ;
	
	// describe query
	Query query = QueryFactory.create(queryString) ;
	QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
	Model resultModel = qexec.execDescribe() ;
	qexec.close() ;

### ASK ###

* returns a boolean indicating whether the query returned at least one result

#### Code example ####

	Query query = QueryFactory.create(queryString) ;
	QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
	boolean result = qexec.execAsk() ;
	qexec.close() ;
	
### Datasets for query ###

* examples above query only one RDF graph (^= Jena Model)
* => create a dataset with a default graph and multiple named graphs

#### Code example ####

	Dataset dataset = DatasetFactory.create() ;
	dataset.setDefaultModel(model) ;
	dataset.addNamedModel("http://example/named-1", modelX) ;
	dataset.addNamedModel("http://example/named-2", modelY) ;
	 try(QueryExecution qExec = QueryExecutionFactory.create(query, dataset)) {
	    ...
	}

* advanced features:
  - programmatically create / change SPARQL queries (syntax vs. algebra level)
  - provide custom extensions for ARQ (custom FILTER functions, "magic properties" (custom property functions to introduce custom query stages), DESCRIBE handlers, extend query evaluation)
