# SPARQL notes #

SPARQL = RDF query language and data access protocol

consists of 3 W3C recommendations: 
- *SPARQL Query Language specification* = core
- *SPARQL Query XML Results Format* = XML format for serializing query results (for SELECT + ASK queries)
- *SPARQL Protocol for RDF specification* uses WSDL 2.0 to define simple HTTP and SOAP protocols for remote querying RDF databases

## RDF Data Store = RDF Database = Triplet Store ##

- special database system built for storage/retrieval of RDF statements (subject-predicate-object triplets)
- optimized for RDF data

features:
- common storage medium for any application that manipulates RDF content
- set of APIs that allow applications to add triplets, query and delete triplets from store

endpoint = accept queries and return results

_generic_ (works against any RDF dataset that could be stored locally or accessible via URL) 
  vs.
_specific_ endpoints (tied to one particular dataset that cannot be switched to another endpoint)

## SPARQL Query Language ##

4 different forms of query:
- SELECT
- ASK
- DESCRIBE
- CONSTRUCT

all queries are based on 2 basic SPARQL concepts: *triplet pattern* and *graph pattern*

### Triplet Pattern ###

- like RDF, SPARQL is built upon the triplet concept _subject_,_predicte_,_object_ .
- can include variables (variable subject, predicate, object or even all are variable): `?varName` (or `$varName`)
  => placeholder that can match every value

- Tries to match the pattern against a collection of RDF triplets, and binds the variables according to the match
<http://danbri.org/foaf.rdf#danbri> foaf:name ?name. => name(s)
<http://danbri.org/foaf.rdf#danbri> ?property ?name. => all properties for subject
?subject ?property ?name. => all RDF triplets

### Graph Pattern ###

- like triplet pattern used to select triplets from a RDF graph
- but allows to specify much more complex selection rules compared to triplet patterns
- collection of triplet patterns = graph pattern
- {} are used to specify a graph pattern
- example:

	{
		?who foaf:name ?name.
		?who foaf:interest ?interest.
		?who foaf:knows ?others.
	}

- for every RDF triplet in the graph:
  - check first triplet pattern (if no result, continue with next RDF triplet)
  - check second triplet pattern (if no result, continue with next RDF triplet)
  - ...
  - add triplet to result set

- variable is bound to the same value (e.g. ?who represents the same value in all triplet patterns of the graph pattern)

- logical AND connection between triplet patterns

### SELECT query ###

- used to construct standard queries
- structure:
	
	# base directive
	BASE <URI>
	# list of prefixes
	PREFIX pref: <URI>
	# result description
	SELECT ...
	# graph to search
	FROM ...
	# query pattern
	WHERE {
		...
	}
	# query modifiers
	ORDER BY | LIMIT | ...

- base directive + list of prefixes = URL abbreviations (optional)
- SELECT: specify which variable bindings (data items) to return
- FROM: tells the SPARQL endpoint which graph to execute the query against
  - sometimes optional <TODO>
- WHERE: specifies the graph pattern for the desired results
  - WHERE clause is mandatory (though the WHERE keyword isn't, but should not be removed for readability)
- query modifiers (like ORDER BY, LIMIT) (optional)

	base <http://danbri.org/foaf.rdf>
	prefix foaf: <http://xmlns.com/foaf/0.1/>
	select *
	from <http://danbri.org/foaf.rdf>
	where
	{
	  <#danbri> foaf:knows ?friend.
	  ?friend foaf:name ?name.
	  ?friend foaf:mbox ?mbox.
	  ?friend foaf:homepage ?homepage.
	}

- _object-to-subject transfer_ (e.g. above: `?friend`) allows to traverse multiple links in the graph
  => knowledge of the underlying ontologies is needed

- `optional` keyword
  - used because RDF is only a semi-structured data model

	base <http://danbri.org/foaf.rdf>
	prefix foaf: <http://xmlns.com/foaf/0.1/>
	select *
	from <http://danbri.org/foaf.rdf>
	where
	{
	  <#danbri> foaf:knows ?friend.
	  optional { ?friend foaf:name ?name. }
	  optional { ?friend foaf:mbox ?mbox. }
	  optional { ?friend foaf:homepage ?homepage. }
	}

- search will try to match all triplet patterns, but will not fail for optional ones
- `optional` can contain multiple triplet patterns => each triplet pattern must match, e.g.

	base <http://danbri.org/foaf.rdf>
	prefix foaf: <http://xmlns.com/foaf/0.1/>
	select *
	from <http://danbri.org/foaf.rdf>
	where
	{
	  <#danbri> foaf:knows ?friend.
	  optional { 
	    ?friend foaf:name ?name.
		?friend foaf:mbox ?mbox.
		?friend foaf:homepage ?homepage.
	  }
	}

=> all the information in the optional block must be present or all must be absent

- SPARQL engine tries to match triplets contained in the graph patterns against the RDF graph 
=> binds variables to the graph's nodes => one binding is called _query solution_
=> one result row is called _solution_

- _Solution Modifiers_
  - select *distinct*
  - *order by* with *asc()* / *desc()*
  - *offset* + *limit*

- *FILTER*
  - filter result using constraints (boolean expressions)
  - combined expressions with boolean operations and `&&` and or `||`
  - example:
  
	base <http://danbri.org/foaf.rdf>
	prefix foaf: <http://xmlns.com/foaf/0.1/>
	select distinct ?property ?propertyValue
	from <http://danbri.org/foaf.rdf>
	where
	{
	  ?timB foaf:name ?y.
	  ?timB ?property ?propertyValue.
	  filter regex(str(?y), "tim berners-Lee", "i").
	}
	
  - (frequently used) functions and operations:
    - (Logical) !, &&, ||
    - (Math) +, -, *, /
    - (Comparison) =, !=, <, >
    - (SPARQL Testers) isURL(), isBlank(), isLiteral(), bound()
    - (SPARQL Accessors) str(), lang(), datatype()
    - (other) sameTerm(), langMatches(), regex()

	base <http://danbri.org/foaf.rdf>
	prefix foaf: <http://xmlns.com/foaf/0.1/>
	prefix xsd: <http://www.w3.org/2001/XMLSchema#>
	select ?name ?dob
	from <http://danbri.org/foaf.rdf>
	where
	{
		?person a foaf:Person.
		?person foaf:name ?name.
		?person foaf:dateOfBirth ?dob.
		filter ( xsd:date(str(?dob)) >= "1970-01-01"^^xsd:date &&
				 xsd:date(str(?dob)) < "1980-01-01"^^xsd:date )
	}

- *UNION*
  - _alternative match_ situation: multiple (mutually exclusive) graph patterns; any solution must match exactly one graph pattern
  - example:

	base <http://danbri.org/foaf.rdf>
	prefix foaf: <http://xmlns.com/foaf/0.1/>
	select ?name ?mbox
	from <http://danbri.org/foaf.rdf>
	where
	{
		?person a foaf:Person.
		?person foaf:name ?name.
		{
			{ ?person foaf:mbox ?mbox. }
			union
			{ ?person foaf:mbox_sha1sum ?mbox. }
		}
	}

  - difference to optional: returns solutions with properties not set too

- Querying multiple graphs:
  - FROM <uri> => called _background graph_
  - in addition, querying _named graphs_ is possible
  - examples:
  
	prefix foaf: <http://xmlns.com/foaf/0.1/>
	select distinct ?graph_uri ?name ?email
	from named <http://www.liyangyu.com/foaf.rdf>
	from named <http://danbri.org/foaf.rdf>
	where
	{
	   graph ?graph_uri
	   {
	      ?person a foaf:Person.
	      ?person foaf:mbox ?email.
	      optional { ?person foaf:name ?name. }
	   }
	}

	prefix foaf: <http://xmlns.com/foaf/0.1/>
	select distinct ?name ?email
	from named <http://www.liyangyu.com/foaf.rdf>
	from named <http://danbri.org/foaf.rdf>
	where
	{
	   graph <http://www.liyangyu.com/foaf.rdf>
	   {
	      ?person a foaf:Person.
	      ?person foaf:mbox ?email.
	      optional { ?person foaf:name ?name. }
	   }.
	   graph <http://danbri.org/foaf.rdf>
	   {
	      ?person1 a foaf:Person.
	      ?person1 foaf:mbox ?email.
	      optional { ?person1 foaf:name ?name. }
	   }.
	}
=> ?email + ?name are used in both graph sections;
   -> bound in the 1st section and used to find a match in the 2nd section

- combination of background graph / named graphs:

	prefix foaf: <http://xmlns.com/foaf/0.1/>
	select distinct ?property ?hasValue
	from <http://danbri.org/foaf.rdf>
	from named <http://www.liyangyu.com/foaf.rdf>
	from named <http://danbri.org/foaf.rdf>
	where
	{
	   graph <http://www.liyangyu.com/foaf.rdf>
	   {
	     ?person1 a foaf:Person.
	     ?person1 foaf:mbox ?email.
 	     optional { ?person1 foaf:name ?name. }
 	   }
	   graph <http://danbri.org/foaf.rdf>
	   {
	     ?person a foaf:Person.
	     ?person foaf:mbox ?email.
	     optional { ?person foaf:name ?name. }
	   }.
	   ?x a foaf:Person.
	   ?x foaf:mbox ?email.
	   ?x ?property ?hasValue.
	}

### CONSTRUCT query ###

- instead of returning a collection of query results, it returns a new RDF graph
- makes it possible to transform an existing graph into a new graph that uses different ontology
- example

	prefix foaf: <http://xmlns.com/foaf/0.1/>
	construct {
	   ?person a foaf:Person.
	   ?person foaf:name ?name.
	   ?person foaf:mbox ?email.
	}
	from <http://danbri.org/foaf.rdf>
	where {
	   ?person a foaf:Person.
	   ?person foaf:name ?name.
	   ?person foaf:mbox ?email.
	}

### DESCRIBE query ###

- can be used to get further information about a resource
- the query processor returns useful information about the resource
- <output format?>
- example:

	prefix foaf: <http://xmlns.com/foaf/0.1/>
	describe ?x
	from <http://danbri.org/foaf.rdf>
	where
	{
	   ?x foaf:mbox <mailto:timbl@w3.org>.
	}

### ASK query ###

- returns `true` or `false` depending on whether the given graph pattern has any matches in the dataset or not
- example

	prefix foaf: <http://xmlns.com/foaf/0.1/>
	ask
	from <http://danbri.org/foaf.rdf>
	where {
	   ?x foaf:mbox <mailto:danbri@danbri.org>.
	}

## SPARQL 1.1 ##

- Aggregate Functions

- Sub-Queries

- Negotiation (NOT EXISTS / MINUS)

- Expressions with SELECT / projected expressions
